package com.ati.ndc.demo.controller;

import com.ati.ndc.commons.model.airshopping.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping(value = "/ndc")
public class DemoController {

    @RequestMapping(value = "/dummy", method = RequestMethod.POST)
    public void demo(@RequestBody IATAAirShoppingRQ obj) throws JAXBException, ParseException {
        String authKey = "dcqywqyw2nj4su28svt8pnyu";
        String apiUrl = "http://iata.api.mashery.com/athena/ndc192api";
        IATAAirShoppingRQ iataAirShoppingRQ = new IATAAirShoppingRQ();
        iataAirShoppingRQ.setXmlns("http://www.iata.org/IATA/2015/00/2019.2/IATA_AirShoppingRQ");

        MessageDoc messageDoc = new MessageDoc();
        messageDoc.setRefVersionNumber(1.0);

        Aggregator aggregator = new Aggregator();
        aggregator.setAggregatorId("88888888");

        List<Participant> participant = new ArrayList<Participant>();
        Participant participantData = new Participant();
        participantData.setAggregator(aggregator);
        participant.add(participantData);

        TravelAgency travelAgency = new TravelAgency();
        travelAgency.setAgencyId("akbar");
        travelAgency.setIataNumber(12312312);
        travelAgency.setName("Gods Travel");

        PartyType sender = new PartyType();
        sender.setTravelAgency(travelAgency);

        Party party = new Party();
        party.setParticipant(participant);
        party.setSender(sender);

        iataAirShoppingRQ.setMessageDoc(messageDoc);
        iataAirShoppingRQ.setParty(party);

        StringWriter sw = new StringWriter();

        String pattern = "yyyy-MM-dd'T'HH:mm:ssXXX";
        String timestampAsString = "2001-12-17T09:30:47+05:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(timestampAsString));

        Timestamp timestamp = Timestamp.valueOf(localDateTime);
//        assertEquals("2018-11-12 13:02:56.12345678", timestamp.toString());


//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        Date date = dateFormat.parse("2001-12-17T09:30:47+05:00");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
//        System.out.println(sdf.format(date));
//        sdf.format(date);
//        long time = date.getTime();

        PayloadStandardAttributes payloadStandardAttributes = new PayloadStandardAttributes();
        payloadStandardAttributes.setEchoTokenText("echo");
        payloadStandardAttributes.setTimestamp(timestamp);
        payloadStandardAttributes.setTrxId("transaction2");
        payloadStandardAttributes.setVersionNumber(2019.2);

        City city = new City();
        city.setIataLocationCode("ATH");

        Country country = new Country();
        country.setCountryCode("GR");

        POS pos = new POS();
        pos.setCity(city);
        pos.setCountry(country);

//        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        Date postDate = dateFormat.parse("2018-10-12T07:38:00");
        pos.setRequestTime(new Date());

        DestArrivalCriteria destArrivalCriteria1 = new DestArrivalCriteria();
        destArrivalCriteria1.setIataLocationCode("LHR");

        DateFormat dateFormatt = new SimpleDateFormat("yyyy-MM-dd");
        DestArrivalCriteria originDepCriteria1 = new DestArrivalCriteria();
        originDepCriteria1.setDate(dateFormatt.parse("2020-06-20"));
        originDepCriteria1.setIataLocationCode("BCN");

        OriginDestCriteria originDestCriteria1 = new OriginDestCriteria();
        originDestCriteria1.setDestArrivalCriteria(destArrivalCriteria1);
        originDestCriteria1.setOriginDepCriteria(originDepCriteria1);

        DestArrivalCriteria destArrivalCriteria2 = new DestArrivalCriteria();
        destArrivalCriteria2.setIataLocationCode("BCN");

        DestArrivalCriteria originDepCriteria2 = new DestArrivalCriteria();
        originDepCriteria2.setDate(dateFormatt.parse("2020-06-27"));
        originDepCriteria2.setIataLocationCode("LHR");

        OriginDestCriteria originDestCriteria2 = new OriginDestCriteria();
        originDestCriteria2.setDestArrivalCriteria(destArrivalCriteria2);
        originDestCriteria2.setOriginDepCriteria(originDepCriteria2);

        List<OriginDestCriteria> originDestCriteriaList = new ArrayList<>();
        originDestCriteriaList.add(originDestCriteria1);
        originDestCriteriaList.add(originDestCriteria2);

        FlightRequestType flightRequest = new FlightRequestType();
        flightRequest.setOriginDestCriteria(originDestCriteriaList);

        PaxRQ pax1 = new PaxRQ();
        pax1.setPaxId("Pax1");
        pax1.setPTC("ADT");

        PaxRQ pax2 = new PaxRQ();
        pax2.setPaxId("Pax1");
        pax2.setPTC("ADT");

        List<PaxRQ> paxList = new ArrayList<>();
        paxList.add(pax1);
        paxList.add(pax2);

        Paxs paxs = new Paxs();
        paxs.setPax(paxList);

        CabinType cabinTypeCriteria = new CabinType();
        cabinTypeCriteria.setCabinTypeCode("M");

        List<CabinType> cabinTypeCriteriaList = new ArrayList<>();
        cabinTypeCriteriaList.add(cabinTypeCriteria);

        ShoppingCriteria shoppingCriteria = new ShoppingCriteria();
        shoppingCriteria.setCabinTypeCriteria(cabinTypeCriteriaList);

        Request request = new Request();
        request.setFlightRequest(flightRequest);
        request.setPaxs(paxs);
        request.setShoppingCriteria(shoppingCriteria);

        iataAirShoppingRQ.setPayloadAttributes(payloadStandardAttributes);
        iataAirShoppingRQ.setRequest(request);
        iataAirShoppingRQ.setPos(pos);

        JAXBContext context = JAXBContext.newInstance(IATAAirShoppingRQ.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(iataAirShoppingRQ, sw);
        System.out.println(sw.toString());

        RestTemplate restTemplate =  new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/xml");
        headers.add("Authorization-Key", authKey);

        HttpEntity<String> requestBody = new HttpEntity<String>(sw.toString(),headers);
        ResponseEntity<String> response = restTemplate.postForEntity(apiUrl, requestBody, String.class);

        if(response.getStatusCode().value() == 200 && response.getBody().contains("Errors")) {
            System.out.println("berhas");
        }
    }
}