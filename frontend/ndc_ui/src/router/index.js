import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import AirlineProfile from '../views/AirlineProfile.vue'
import AirlineProfileNotif from '../views/AirlineProfileNotif.vue'
import AirShopping from '../views/AirShopping.vue'
import INVReleaseNotif from '../views/INVReleaseNotif.vue'
import OfferPrice from '../views/OfferPrice.vue'
import SeatAvailbility from '../views/SeatAvailbility.vue'
import ServiceList from '../views/ServiceList.vue'
import OrderRules from '../views/OrderRules.vue'
import OrderReshop from '../views/OrderReshop.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  {
    path: '/airlineProfile',
    name: 'AirlineProfile',
    component: AirlineProfile
  },
  {
    path: '/airlineProfileNotif',
    name: 'AirlineProfileNotif',
    component: AirlineProfileNotif
  },
  {
    path: '/airShopping',
    name: 'AirShopping',
    component: AirShopping
  },
  {
    path: '/invReleaseNotif',
    name: 'INVReleaseNotif',
    component: INVReleaseNotif
  },
  {
    path: '/offerPrice',
    name: 'OfferPrice',
    component: OfferPrice
  },
  {
    path: '/seatAvailbility',
    name: 'SeatAvailbility',
    component: SeatAvailbility
  },
  {
    path: '/serviceList',
    name: 'ServiceList',
    component: ServiceList
  },
  {
    path: '/orderRules',
    name: 'OrderRules',
    component: OrderRules
  },
  {
    path: '/orderReshop',
    name: 'OrderReshop',
    component: OrderReshop
  }
]

const router = new VueRouter({
  routes
})

export default router
