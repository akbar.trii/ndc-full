package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "OriginDestCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class OriginDestCriteria {
    @XmlElement(name = "CalendarDateCriteria")
    private CalendarDateCriteria CalendarDateCriteria;
    @XmlElement(name = "ConnectionPrefRefID")
    private String connectionPrefRefId;
    @XmlElement(name = "DestArrivalCriteria")
    private DestArrivalCriteria destArrivalCriteria; //cant be null
    @XmlElement(name = "JourneyDurationCriteria")
    private JourneyDurationCriteria journeyDurationCriteria;
    @XmlElement(name = "OriginDepCriteria")
    private DestArrivalCriteria originDepCriteria;  //cant be null, and variable Date cant be null
    @XmlElement(name = "OriginDestID")
    private String originDestId;
    @XmlElement(name = "PreferredCabinType")
    private List<CabinType> preferredCabinType;
}
