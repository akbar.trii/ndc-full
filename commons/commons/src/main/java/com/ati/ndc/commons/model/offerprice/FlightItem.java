package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FlightItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightItem {
    @XmlElement(name = "FareDetail")
    private FareDetailRQ fareDetail;
    @XmlElement(name = "OriginDestRefID")
    private List<String> originDestRefId; //cant be null
    @XmlElement(name = "Price")
    private Price price;
}
