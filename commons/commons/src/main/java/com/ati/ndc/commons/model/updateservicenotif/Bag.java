package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Bag")
@XmlAccessorType(XmlAccessType.FIELD)
public class Bag {
    @XmlElement(name = "BagDimension")
    private BagDimension bagDimension;
    @XmlElement(name = "BagTag")
    private BagTag bagTag;
}
