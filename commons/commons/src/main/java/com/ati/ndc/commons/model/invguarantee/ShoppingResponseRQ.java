package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ShoppingResponseRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShoppingResponseRQ {
        @XmlElement(name = "OwnerCode")
        private String ownerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
        @XmlElement(name = "ShoppingResponseRefID")
        private String shoppingResponseRefId;
}
