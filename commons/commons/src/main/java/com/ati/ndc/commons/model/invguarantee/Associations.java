package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Associations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Associations {
    @XmlElement(name = "Offer")
    private OfferRS offer;
    @XmlElement(name = "Order")
    private OrderRS order;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponseRQ shoppingResponse;
}
