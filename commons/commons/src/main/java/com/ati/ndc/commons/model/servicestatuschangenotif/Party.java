package com.ati.ndc.commons.model.servicestatuschangenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Party")
@XmlAccessorType(XmlAccessType.FIELD)
public class Party {
    @XmlElement(name = "Participant")
    private List<Participant> participant;
    @XmlElement(name = "Recipient")
    private PartyType recipient;
    @XmlElement(name = "Sender")
    private PartyType sender;   //cant be null
}
