package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DataListsRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class DataListsRS {
    @XmlElement(name = "BaggageAllowanceList")
    private BaggageAllowanceList baggageAllowanceList;
    @XmlElement(name = "BaggageDisclosureList")
    private BaggageDisclosureList baggageDisclosureList;
    @XmlElement(name = "ContactInfoList")
    private ContactInfoList contactInfoList;
    @XmlElement(name = "DisclosureList")
    private DisclosureList disclosureList;
    @XmlElement(name = "FareList")
    private FareListRS fareList;
    @XmlElement(name = "MediaList")
    private MediaList mediaList;
    @XmlElement(name = "OriginDestList")
    private OriginDestList originDestList;
    @XmlElement(name = "PaxJourneyList")
    private PaxJourneyListRS paxJourneyList;
    @XmlElement(name = "PaxList")
    private PaxListRS paxList;
    @XmlElement(name = "PaxSegmentList")
    private PaxSegmentListRS paxSegmentList;
    @XmlElement(name = "PenaltyList")
    private PenaltyListRS penaltyList;
    @XmlElement(name = "PriceClassList")
    private PriceClassList priceClassList;
    @XmlElement(name = "SeatProfileList")
    private SeatProfileList seatProfileList;
    @XmlElement(name = "ServiceDefinitionList")
    private ServiceDefinitionListRS serviceDefinitionList;
    @XmlElement(name = "TermsList")
    private TermsList termsList;
}
