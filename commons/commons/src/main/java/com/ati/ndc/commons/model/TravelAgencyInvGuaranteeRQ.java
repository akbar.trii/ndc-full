package com.ati.ndc.commons.model.invguarantee;

import com.ati.ndc.commons.model.enumeration.TypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "TravelAgencyInvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class TravelAgencyInvGuaranteeRQ {
    @XmlElement(name = "AgencyID")
    private String agencyId;    //cant be null
    @XmlElement(name = "ContactInfo")
    private List<ContactInfo> contactInfo;
    @XmlElement(name = "IATA_Number")
    private Integer iataNumber;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "PseudoCityID")
    private String pseudoCityId;
    @XmlElement(name = "TravelAgent")
    private TravelAgent travelAgent;
    @XmlElement(name = "TypeCode")
    private TypeCode typeCode;
}
