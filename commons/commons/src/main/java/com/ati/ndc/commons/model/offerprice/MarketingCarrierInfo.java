package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "MarketingCarrierInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class MarketingCarrierInfo {
    @XmlElement(name = "CarrierDesigCode")
    private String carrierDesigCode;    //token with pattern value="([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])" and cant be null
    @XmlElement(name = "CarrierName")
    private String carrierName;
    @XmlElement(name = "MarketingCarrierFlightNumberText")
    private String marketingCarrierFlightNumberText;    //cant be null
    @XmlElement(name = "OperationalSuffixText")
    private String operationalSuffixText;
    @XmlElement(name = "RBD_Code")
    private String rbdCode;
}
