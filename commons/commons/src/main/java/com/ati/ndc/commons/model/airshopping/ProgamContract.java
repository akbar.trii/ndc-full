package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ProgamContract")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProgamContract {
    @XmlElement(name = "ContractID")
    private String contractId;  //cant be null
}
