package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DirectBill {
    private String contactInfoRefId;    //token
    private String orgId;   //token
    private String orgName;
}