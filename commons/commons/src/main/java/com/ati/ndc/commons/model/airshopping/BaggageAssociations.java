package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BaggageAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BaggageAssociations {
    @XmlElement(name = "BaggageAllowanceRefID")
    private String baggageAllowanceRefId;   //cant be null
    @XmlElement(name = "BaggageFlightAssociations")
    private ServiceDefinitionFlightAssociations baggageFlightAssociations;
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;    //cant be null
}
