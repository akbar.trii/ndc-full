package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GeneralAssociation")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class GeneralAssociation {
    @XmlElement(name = "AssociatedObjectID")
    private String associatedObjectId;
    @XmlElement(name = "AssociatedObjectName")
    private String associatedObjectName;
    @XmlElement(name = "AssociatedObjectPath")
    private String associatedObjectPath;
}