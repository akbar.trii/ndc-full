package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PolicyNodeInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PolicyNodeInfo {
    @XmlElement(name = "PathText")
    private String pathText;
    @XmlElement(name = "TagName")
    private String tagName;
}