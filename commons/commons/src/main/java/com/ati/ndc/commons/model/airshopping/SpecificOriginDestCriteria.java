package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "SpecificOriginDestCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecificOriginDestCriteria {
    @XmlElement(name = "DestStationCode")
    private String destStationCode;     //token with length 3 and cant be null
    @XmlElement(name = "OriginDestID")
    private String originDestId;
    @XmlElement(name = "OriginStationCode")
    private String originStationCode;   //token with length 3 and cant be null
    @XmlElement(name = "PaxJourney")
    private List<PaxJourneyAirShoppingRQ> paxJourney;
}
