package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AlaCarteOfferItemPaymentTimeLimit")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AlaCarteOfferItemPaymentTimeLimit {
    //choices
    @XmlElement(name = "paymentTimeLimitDate")
    private PaymentTimeLimitDateType paymentTimeLimitDate;
    @XmlElement(name = "PaymentTimeLimitDuration")
    private PaymentTimeLimitDurationType paymentTimeLimitDuration;
}
