package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.DeviceOwnerTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PaymentMethodCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethodCriteria {
    @XmlElement(name = "IATA_EasyPayCriteria")
    private IATAEasyPayCriteria iataEasyPayCriteria;
    @XmlElement(name = "OfferAssociation")
    private List<OfferAssociation> offerAssociation;
    @XmlElement(name = "PaymentCardCriteria")
    private PaymentCardCriteria paymentCardCriteria;
    @XmlElement(name = "PaymentInstrumentOwnershipCode")
    private DeviceOwnerTypeCode paymentInstrumentOwnershipCode;
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
    @XmlElement(name = "TypeCode")
    private String typeCode;    //cant be null
}
