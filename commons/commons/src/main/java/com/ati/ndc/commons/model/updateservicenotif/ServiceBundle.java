package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceBundle")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceBundle {
    @XmlElement(name = "MaximumServiceQty")
    private Double maximumServiceQty;
    @XmlElement(name = "ServiceDefinitionRefID")
    private List<String> serviceDefinitionRefId;  //cant be null
}