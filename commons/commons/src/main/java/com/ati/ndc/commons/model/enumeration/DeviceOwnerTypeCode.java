package com.ati.ndc.commons.model.enumeration;

public enum DeviceOwnerTypeCode {
    CS, //Customer
    SL, //Seller
}
