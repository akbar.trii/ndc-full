package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PaymentCardCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentCardCriteria {
    @XmlElement(name = "CardBrandCode")
    private String cardBrandCode;   //cant be null
    @XmlElement(name = "CardIssuingCountryCode")
    private String cardIssuingCountryCode;  //token with pattern value [A-Z]{2}
    @XmlElement(name = "CardProductTypeCode")
    private String cardProductTypeCode;
    @XmlElement(name = "IssuerIdentificationNumber")
    private String issuerIdentificationNumber;  //string with pattern [0-9]{1,8}
    @XmlElement(name = "SecurePaymentAuthenticationVersion")
    private SecurePaymentAuthenticationVersion securePaymentAuthenticationVersion;
}