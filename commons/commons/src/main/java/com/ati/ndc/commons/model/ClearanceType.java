package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClearanceType {
    private List<String> airlineRefId;    //token minLength 1 maxLength 15 with pattern value ([A-Za-z0-9]{1,15})
    private List<String> clearanceId;   //token length 15 with pattern ([0-9]{7}[A-Za-z0-9]{8}) and cant be null
    private CommitmentToPay commitmentToPay;    //cant be null
    private Double nonSettledAmount;
//    private PromotionIssuer payee;  //cant be null
//    private PromotionIssuer payer;  //cant be null
    private String processRuleCode; //token length value 7 with pattern (IATAC|BLTRL)[A-Za-z0-9]{2} and cant be null
    private Settlement settlement;  //cant be null
    private String statusCode;  //token length 5 with pattern [A-Za-z0-9]{5} and cant be null
}