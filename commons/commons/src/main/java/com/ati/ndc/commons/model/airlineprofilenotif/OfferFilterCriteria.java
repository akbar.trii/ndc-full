package com.ati.ndc.commons.model.airlineprofilenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OfferFilterCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferFilterCriteria {
    @XmlElement(name = "DirectionalIndText")
    private String directionalIndText;  // example value (1 = From LOC 1 to LOC 2, 2 = To LOC 1 from LOC 2, 3 = Both)
    @XmlElement(name = "OfferDestPoint")
    private POSGeographicFilterCriteria offerDestPoint;
    @XmlElement(name = "OfferOriginPoint")
    private POSGeographicFilterCriteria offerOriginPoint;
    @XmlElement(name = "TravelWithin")
    private POSGeographicFilterCriteria travelWithin;
}