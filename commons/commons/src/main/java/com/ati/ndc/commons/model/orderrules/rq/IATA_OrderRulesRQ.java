package com.ati.ndc.commons.model.orderrules.rq;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_OrderRulesRQ extends IATA_OrderChangeRQ {
}