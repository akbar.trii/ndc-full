package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Org")
@XmlAccessorType(XmlAccessType.FIELD)
public class Org {
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "OrgID")
    private String orgId;    //cant be null
}