package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.FlightCharacteristicCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "FlightCharacteristicsCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class FlightCharacteristicsCriteria {
    @XmlElement(name = "CharacteristicsCode")
    private FlightCharacteristicCodeType characteristicsCode;   //cant be null
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;    //cant be null
}
