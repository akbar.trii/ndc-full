package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "ServiceDefinition")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinition {
    @XmlElement(name = "BookingInstructions")
    private BookingInstructions bookingInstructions;
    @XmlElement(name = "DepositTimeLimitDateTime")
    private Date depositTimeLimitDateTime;
    @XmlElement(name = "Desc")
    private List<Desc> desc;  //cant be null
    @XmlElement(name = "Name")
    private String name;    //cant be null
    @XmlElement(name = "NamingTimeLimitDateTime")
    private Date namingTimeLimitDateTime;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "RFIC")
    private String rfic;    //value pattern [0-9A-Z]{1,3}
    @XmlElement(name = "RFISC")
    private String rfisc;
    @XmlElement(name = "ServiceCode")
    private String serviceCode;
    @XmlElement(name = "ServiceDefinitionAssociation")
    private ServiceDefinitionAssociation serviceDefinitionAssociation;
    @XmlElement(name = "ServiceDefinitionID")
    private String serviceDefinitionId; //cant be null
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
}