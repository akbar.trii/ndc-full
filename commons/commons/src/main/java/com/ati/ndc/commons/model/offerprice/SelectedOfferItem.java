package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SelectedOfferItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectedOfferItem {
    @XmlElement(name = "OfferItemRefID")
    private String offerItemRefId;  //cant be null
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;  //cant be null
    @XmlElement(name = "SelectedALaCarteOfferItem")
    private SelectedALaCarteOfferItem selectedALaCarteOfferItem;  //cant be null
    @XmlElement(name = "SelectedBundleServices")
    private SelectedBundleServices selectedBundleServices;  //cant be null
    @XmlElement(name = "SelectedSeat")
    private SelectedSeat selectedSeat;  //cant be null
}
