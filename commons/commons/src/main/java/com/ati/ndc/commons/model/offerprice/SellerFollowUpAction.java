package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.ConsequenceOfInactionCodeType;
import com.ati.ndc.commons.model.enumeration.SellerFollowupActionCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@XmlRootElement(name = "SellerFollowUpAction")
@XmlAccessorType(XmlAccessType.FIELD)
public class SellerFollowUpAction {
    @XmlElement(name = "ActionCode")
    private List<SellerFollowupActionCodeType> actionCode;
    @XmlElement(name = "ActionInd")
    private boolean actionInd;
    @XmlElement(name = "ActionTimeLimitDateTime")
    private Date actionTimeLimitDateTime;
    @XmlElement(name = "AirlineContactURI")
    private String airlineContactURI;
    @XmlElement(name = "ConsequenceOfInactionCode")
    private ConsequenceOfInactionCodeType consequenceOfInactionCode;
}
