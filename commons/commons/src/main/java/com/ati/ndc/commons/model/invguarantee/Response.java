package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    @XmlElement(name = "InventoryGuarantee")
    private List<InventoryGuarantee> inventoryGuarantee;
    @XmlElement(name = "InventoryGuaranteeProcessing")
    private InventoryGuaranteeProcessing inventoryGuaranteeProcessing;
    @XmlElement(name = "NoGuaranteeInd")
    private Boolean noGuaranteeInd;
    @XmlElement(name = "Warning")
    private List<Warning> warning;
}
