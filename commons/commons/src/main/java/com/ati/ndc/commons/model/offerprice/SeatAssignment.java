package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.airshopping.Seat;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SeatAssignment")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class SeatAssignment {
    @XmlElement(name = "DatedOperatingLegRefID")
    private String DatedOperatingLegRefId;  //cant be null
    @XmlElement(name = "Seat")
    private Seat seat;  //cant be null
    @XmlElement(name = "ServiceDefinitionRefID")
    private String serviceDefinitionRefId;
}
