package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Arrival")
@XmlAccessorType(XmlAccessType.FIELD)
public class Arrival {
    @XmlElement(name = "AircraftScheduledDateTime")
    private Date aircraftScheduledDateTime;
    @XmlElement(name = "BoardingGateID")
    private String boardingGateId; //token
    @XmlElement(name = "IATA_LocationCode")
    private String iataLocationCode; //token && length = 3
    @XmlElement(name = "StationName")
    private String stationName; //token
    @XmlElement(name = "TerminalName")
    private String terminalName; //token
}
