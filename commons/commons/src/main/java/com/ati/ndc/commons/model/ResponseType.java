package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_OfferPriceRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseType {
//    @XmlElement(name = "DataLists")
//    private Datalists dataLists;
//    @XmlElement(name = "MessageProcessing")
//    private MessageProcessing messageProcessing;
//    @XmlElement(name = "Metadata")
//    private Metadata metadata;
    @XmlElement(name = "OtherOffers")
    private OtherOffers otherOffers;
    @XmlElement(name = "PaymentFunctions")
    private PaymentFunctionsType paymentFunctions;
//    @XmlElement(name = "Policy")
//    private List<Policy> policy;
    @XmlElement(name = "PricedOffer")
    private Offer pricedOffer;    //cant be null
    @XmlElement(name = "Promotion")
    private List<PromotionType> promotion;
    @XmlElement(name = "ResponseParameters")
    private ResponseParameters responseParameters;
//    @XmlElement(name = "ShoppingResponse")
//    private ShoppingResponse shoppingResponse;
//    @XmlElement(name = "Warning")
//    private List<Warning> warning;
}