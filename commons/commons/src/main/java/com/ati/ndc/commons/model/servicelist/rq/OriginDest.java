package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OriginDest")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OriginDest {
    @XmlElement(name = "DestStationCode")
    private String destStationCode;    //token with length 3 and cant be null
    @XmlElement(name = "OriginDestID")
    private String originDestId;
    @XmlElement(name = "OriginStationCode")
    private String originStationCode;  //token with length 3 and cant be null
    @XmlElement(name = "PaxJourney")
    private List<PaxJourney> paxJourney;
    @XmlElement(name = "SegmentDurationCriteria")
    private SegmentDurationCriteria segmentDurationCriteria;
}