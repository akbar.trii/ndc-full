package com.ati.ndc.commons.model;

import lombok.Data;

import java.util.List;

@Data
public class PromotionType {
    private String ownerName;
    private List<String> paxRefId;
    private String promotionId; //cant be null
//    private PromotionIssuer promotionIssuer;
//    private List<Remark> remark;
    private String url;
}
