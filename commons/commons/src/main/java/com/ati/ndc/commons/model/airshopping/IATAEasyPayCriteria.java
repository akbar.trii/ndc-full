package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "IATA_EasyPayCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAEasyPayCriteria {
    @XmlElement(name = "AccountNumber")
    private String accountNumber;
    @XmlElement(name = "ExpirationDate")
    private String expirationDate;  //String with pattern value (0[1-9]|1[0-2])[0-9][0-9]
    @XmlElement(name = "IATA_EasyPayEncryptedData")
    private IATAEasyPayEncryptedData iataEasyPayEncryptedData;
    @XmlElement(name = "SettlementData")
    private SettlementData settlementData;
}
