package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "MediaList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class MediaList {
    @XmlElement(name = "Media")
    private List<MediaObject> media;  //cant be null
}