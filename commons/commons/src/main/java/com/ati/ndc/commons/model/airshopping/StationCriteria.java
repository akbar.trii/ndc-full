package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "StationCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class StationCriteria {
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
    @XmlElement(name = "Station")
    private List<Station> station;
}