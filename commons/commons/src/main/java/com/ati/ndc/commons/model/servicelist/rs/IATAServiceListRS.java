package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "IATA_ServiceListRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAServiceListRS {
    // one of these element shud has value (choice)
    @XmlElement(name = "Error")
    private List<Error> error;
    @XmlElement(name = "Response")
    private Response response;
    //end

    @XmlElement(name = "AugmentationPoint")
    private String augmentationPoint;   //any element // still wrong datatype
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
}