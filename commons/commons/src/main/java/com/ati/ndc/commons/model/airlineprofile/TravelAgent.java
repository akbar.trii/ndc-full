package com.ati.ndc.commons.model.airlineprofile;

import com.ati.ndc.commons.model.enumeration.TypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "TravelAgent")
@XmlAccessorType(XmlAccessType.FIELD)
public class TravelAgent {
    @XmlElement(name = "TravelAgentID")
    private String travelAgentId;   //cant be null
    @XmlElement(name = "TypeCode")
    private TypeCode typeCode;
}