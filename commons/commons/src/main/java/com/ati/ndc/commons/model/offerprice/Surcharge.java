package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Surcharge")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Surcharge {
    @XmlElement(name = "AllRefundableInd")
    private Boolean allRefundableInd;
    @XmlElement(name = "Breakdown")
    private List<Fee> breakdown;
    @XmlElement(name = "TotalAmount")
    private Double totalAmount;
}
