package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "GeoSpatialLocation")
@XmlAccessorType(XmlAccessType.FIELD)
public class GeoSpatialLocation {
    @XmlElement(name = "NaturalAreaGeocode")
    private String naturalAreaGeocode;
    @XmlElement(name = "PointElevationNumber")
    private Double pointElevationNumber;
    @XmlElement(name = "PointLatitudeNumber")
    private Double pointLatitudeNumber;
    @XmlElement(name = "PointLongitudeNumber")
    private Double pointLongitudeNumber;
}
