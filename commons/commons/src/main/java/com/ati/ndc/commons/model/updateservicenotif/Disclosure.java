package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Disclosure")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Disclosure {
    @XmlElement(name = "Desc")
    private List<Desc> desc;  //cant be null
    @XmlElement(name = "DisclosureID")
    private String disclosureId;    //cant be null
}