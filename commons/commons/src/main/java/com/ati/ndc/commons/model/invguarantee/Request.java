package com.ati.ndc.commons.model.invguarantee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
    @XmlElement(name = "ShoppingResponse")
    private List<BookingRef> bookingRef;
    @XmlElement(name = "Offer")
    private OfferRQ Offer;
    @XmlElement(name = "Order")
    private OrderRQ Order;
    @XmlElement(name = "Qualifiers")
    private Qualifiers qualifiers;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponseRS shoppingResponse;
}