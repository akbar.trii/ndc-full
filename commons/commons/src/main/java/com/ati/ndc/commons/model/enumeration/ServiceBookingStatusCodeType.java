package com.ati.ndc.commons.model.enumeration;

public enum ServiceBookingStatusCodeType {
    CONFIRMED,
    REQUESTED
}
