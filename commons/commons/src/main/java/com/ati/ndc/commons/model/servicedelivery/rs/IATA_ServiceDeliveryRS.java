package com.ati.ndc.commons.model.servicedelivery.rs;

import com.ati.ndc.commons.model.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_ServiceDeliveryRS {
    // one of these element shud has value (choice)
    private List<Error> error;
    private Response response;
    //end

    private String augmentationPoint;   //any element // still wrong datatype
//    private Party party;    //cant be null
//    private PayloadStandardAttributes payloadAttributes;

    //lihat xml lagi karna masih ada tambahan
}