package com.ati.ndc.commons.model.servicestatuschangenotif;

import com.ati.ndc.commons.model.orderclosingnotif.IATA_OrderClosingNotifRQ;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "IATA_ServiceStatusChangeNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAServiceStatusChangeNotifRQ {
    @XmlElement(name = "AugmentationPoint")
    private String augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "Party")
    private Party party;  //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "Request")
    private Request request;    //cant be null
}