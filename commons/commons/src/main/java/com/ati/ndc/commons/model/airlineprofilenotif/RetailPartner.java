package com.ati.ndc.commons.model.airlineprofilenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "RetailPartner")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetailPartner {
    @XmlElement(name = "ContactInfo")
    private ContactInfo contactInfo;    //cant be null
    @XmlElement(name = "Name")
    private String Name;
    @XmlElement(name = "RetailPartnerID")
    private String retailPartnerId; //cant be null
}