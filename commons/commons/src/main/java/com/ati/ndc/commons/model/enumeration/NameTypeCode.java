package com.ati.ndc.commons.model.enumeration;

public enum NameTypeCode {
    FATHER_SURNAME,
    MOTHER_SURNAME
}
