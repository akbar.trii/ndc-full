package com.ati.ndc.commons.model.invreleasenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "InventoryGuarantee")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryGuarantee {
    @XmlElement(name = "Associations")
    private Associations sssociations;
    @XmlElement(name = "InventoryGuaranteeID")
    private String inventoryGuaranteeId;    //cant be null
    @XmlElement(name = "InventoryGuaranteeTimeLimitDateTime")
    private Date inventoryGuaranteeTimeLimitDateTime;
    @XmlElement(name = "WaitListInd")
    private Boolean waitListInd;
}