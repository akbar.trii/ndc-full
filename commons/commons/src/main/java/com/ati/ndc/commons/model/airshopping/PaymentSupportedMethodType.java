package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaymentSupportedMethodType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaymentSupportedMethodType {
    @XmlElement(name = "OfferAssociation")
    private List<OfferAssociation> offerAssociation;
    @XmlElement(name = "OtherPaymentMethod")
    private OtherPaymentMethodType OtherPaymentMethod;
    @XmlElement(name = "PaymentCard")
    private PaymentCardType paymentCard;
    @XmlElement(name = "PaymentRedirection")
    private PaymentRedirectionType paymentRedirection;
    @XmlElement(name = "SurchargeInfo")
    private SurchargeInfo surchargeInfo;
    @XmlElement(name = "TypeCode")
    private String typeCode;    //cant be null
}
