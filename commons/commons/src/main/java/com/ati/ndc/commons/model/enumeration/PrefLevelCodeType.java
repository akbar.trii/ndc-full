package com.ati.ndc.commons.model.enumeration;

public enum PrefLevelCodeType {
    Exclude,
    Preferred,
    Required,   //female description
}
