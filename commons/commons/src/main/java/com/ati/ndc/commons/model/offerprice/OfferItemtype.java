package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OfferItemtype")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferItemtype {
    //choices
    @XmlElement(name = "BaggageItem")
    private BaggageItem baggageItem;
    @XmlElement(name = "FlightItem")
    private FlightItem flightItem;
    @XmlElement(name = "OtherItem")
    private OtherItem otherItem;
    @XmlElement(name = "SeatItem")
    private SeatItem seatItem;
}