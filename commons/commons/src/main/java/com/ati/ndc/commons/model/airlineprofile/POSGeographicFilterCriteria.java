package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "POSGeographicFilterCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class POSGeographicFilterCriteria {
    @XmlElement(name = "AreaCodeText")
    private String areaCodeText;
    @XmlElement(name = "CoordRadiusMeasure")
    private Double coordRadiusMeasure;
    @XmlElement(name = "CountryCode")
    private String countryCode;    //token with value pattern [A-Z]{2}
    @XmlElement(name = "CountrySubDivisionCode")
    private String countrySubDivisionCode;  //String with min value = 1 and max value = 3
    @XmlElement(name = "GeospatialLocation")
    private GeoSpatialLocation geospatialLocation;
    @XmlElement(name = "IATA_LocationCode")
    private String iataLocationCode;    //token with max length 3
}