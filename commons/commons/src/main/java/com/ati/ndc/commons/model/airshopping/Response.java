package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Response {
    @XmlElement(name = "AirShoppingProcessing")
    private MessageProcessing airShoppingProcessing;
    @XmlElement(name = "Commission")
    private Commission commission;
    @XmlElement(name = "DataLists")
    private DataLists dataLists;
    @XmlElement(name = "Metadata")
    private String Metadata;
    @XmlElement(name = "OffersGroup")
    private OffersGroup offersGroup;    //cant be null
    @XmlElement(name = "PaymentFunctions")
    private PaymentFunctionsType paymentFunctions;
    @XmlElement(name = "Policy")
    private List<Policy> policy;
    @XmlElement(name = "Promotion")
    private List<Promotion> promotion;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponseRS shoppingResponse;
    @XmlElement(name = "Warning")
    private List<Warning> warning;
}
