package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SelectedOffer")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectedOffer {
    @XmlElement(name = "OfferRefID")
    private String offerRefId;  //cant be null
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "SelectedOfferItem")
    private List<SelectedOfferItem> selectedOfferItem;    //cant be null
    @XmlElement(name = "ShoppingResponseRefID")
    private String shoppingResponseRefId;
    @XmlElement(name = "TotalOfferPriceAmount")
    private Double totalOfferPriceAmount;
}
