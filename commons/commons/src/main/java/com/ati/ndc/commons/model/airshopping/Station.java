package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Station")
@XmlAccessorType(XmlAccessType.FIELD)
public class Station {
    @XmlElement(name = "IATA_LocationCode")
    private String iataLocationCode;    //token with length = 3
    @XmlElement(name = "StationName")
    private String stationName;
}