package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClearanceFilterCriteria {
//    private PromotionIssuer payee;  //cant be null
//    private PromotionIssuer payer;  //cant be null
    private List<String> processRuleCode; //token length 7 with pattern value (IATAC|BLTRL)[A-Za-z0-9]{2}
    private List<RemittanceDate> remittanceDate;
    private List<RemittanceDate> settlementDate;
    private List<String> statusCode;    //token length value 5 with pattern [A-Za-z0-9]{5}
    private List<String> typeCode;  //token
}
