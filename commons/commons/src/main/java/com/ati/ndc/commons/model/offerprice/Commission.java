package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Commission")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Commission {
    @XmlElement(name = "Amount")
    private Double amount;
    @XmlElement(name = "CommissionCode")
    private String commissionCode;
    @XmlElement(name = "Percentage")
    private Double percentage;
    @XmlElement(name = "PercentageAppliedToAmount")
    private Double percentageAppliedToAmount;
    @XmlElement(name = "TaxableInd")
    private Boolean taxableInd;
}
