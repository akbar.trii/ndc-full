package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FeeType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FeeType {
    @XmlElement(name = "Amount")
    private Double amount;
    @XmlElement(name = "ApproximateInd")
    private Boolean approximateInd;
    @XmlElement(name = "DescText")
    private String descText;
}
