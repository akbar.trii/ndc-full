package com.ati.ndc.commons.model.paymentremittancetransfernotif;

import com.ati.ndc.commons.model.Remittance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentRemittanceTransferNotif {
//    private PayloadStandardAttributes payloadStandardAttributes;
    private List<Remittance> remittance;    //cant be null
}