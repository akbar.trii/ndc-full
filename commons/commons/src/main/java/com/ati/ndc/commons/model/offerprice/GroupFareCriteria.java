package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.airshopping.PrefLevel;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "GroupFareCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupFareCriteria {
    @XmlElement(name = "FareBasisCode")
    private List<String> fareBasisCode; //cant be null
    @XmlElement(name = "PaxSegmentRefID")
    private List<String> paxSegmentRefId;
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
    @XmlElement(name = "PTC")
    private String ptc; //pattern value [A-Z]{3}
}
