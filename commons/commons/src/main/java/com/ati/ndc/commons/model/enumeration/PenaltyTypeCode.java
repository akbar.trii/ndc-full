package com.ati.ndc.commons.model.enumeration;

public enum PenaltyTypeCode {
    Cancellation,
    Change,
    NoShow,
    Other,
    Upgrade
}
