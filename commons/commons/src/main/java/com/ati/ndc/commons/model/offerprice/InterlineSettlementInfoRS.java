package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "InterlineSettlementInfoRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class InterlineSettlementInfoRS {
    @XmlElement(name = "Commission")
    private List<Commission> commission;
    @XmlElement(name = "MethodCode")
    private String methodCode;  //cant be null and e.g. AD (Internal Airline Document), DS (Direct Settlement), EA (EMD Associated or Internal Airline Document)
    @XmlElement(name = "SettlementAmount")
    private Double settlementAmount;    //cant be null
    @XmlElement(name = "TaxableInd")
    private Boolean taxableInd;
}