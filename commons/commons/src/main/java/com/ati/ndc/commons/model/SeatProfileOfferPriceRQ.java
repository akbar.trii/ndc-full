package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SeatProfileOfferPriceRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class SeatProfileOfferPriceRQ {
    @XmlElement(name = "CharacteristicCode")
    private List<String> characteristicCode;  //max 99
//    @XmlElement(name = "MarketingInfo")
//    private List<Desc> marketingInfo;
//    @XmlElement(name = "SeatKeywords")
//    private List<SeatKeywordsOfferPriceRQ> seatKeywords;
    @XmlElement(name = "SeatPitchMeasure")
    private Double seatPitchMeasure;
    @XmlElement(name = "SeatProfileID")
    private String seatProfileId;   //cant be null
    @XmlElement(name = "SeatWidthMeasure")
    private Double seatWidthMeasure;
}
