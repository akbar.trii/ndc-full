package com.ati.ndc.commons.model.servicelist.rs;

import com.ati.ndc.commons.model.enumeration.ChangeTypeCode;
import com.ati.ndc.commons.model.enumeration.JourneyStageCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "ChangeRestrictions")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ChangeRestrictions {
    @XmlElement(name = "AllowedModificationInd")
    private Boolean allowedModificationInd;
    @XmlElement(name = "ChangeTypeCode")
    private ChangeTypeCode changeTypeCode;
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "EffectiveDateTime")
    private Date effectiveDateTime;
    @XmlElement(name = "ExpirationDateTime")
    private Date expirationDateTime;
    @XmlElement(name = "Fee")
    private FeeType fee;
    @XmlElement(name = "JourneyStageCode")
    private JourneyStageCode journeyStageCode;
    @XmlElement(name = "PaxRefID")
    private String paxRefId;
    @XmlElement(name = "Refund")
    private Refund refund;
}
