package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.BagRuleCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BaggageDisclosure")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BaggageDisclosure {
    @XmlElement(name = "BaggageDisclosureID")
    private String baggageDisclosureId;
    @XmlElement(name = "BDC")
    private BDC bdc;
    @XmlElement(name = "CheckInChargesInd")
    private Boolean checkInChargesInd;
    @XmlElement(name = "CommercialAgreementID")
    private String commercialAgreementId;
    @XmlElement(name = "DeferralInd")
    private Boolean deferralInd;
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "DescText")
    private List<String> descText;
    @XmlElement(name = "FixedPrePaidInd")
    private Boolean fixedPrePaidInd;
    @XmlElement(name = "RuleTypeCode")
    private BagRuleCode RuleTypeCode;   //cant be null
}