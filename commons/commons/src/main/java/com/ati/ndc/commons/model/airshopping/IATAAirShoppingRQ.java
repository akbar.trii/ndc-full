package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "IATA_AirShoppingRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class IATAAirShoppingRQ {
    @XmlAttribute
    private String xmlns;
    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Party")
    private Party party;  //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "POS")
    private POS pos;
    @XmlElement(name = "Request")
    private Request request;   //cant be null
}