package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CarrierCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class CarrierCriteria {
    @XmlElement(name = "Carrier")
    private Carrier carrier;    //cant be null
    @XmlElement(name = "FlightHaulTypeCode")
    private String flightHaulTypeCode;
    @XmlElement(name = "GeographicalIndTypeCode")
    private String geographicalIndTypeCode;
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
}
