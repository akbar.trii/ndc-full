package com.ati.ndc.commons.model.enumeration;

public enum CurAppCode {
    Actual,
    Conversion,
    Display,
    Other,
    Requested
}
