package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Pax")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Pax {
    @XmlElement(name = "AgeMeasure")
    private Double ageMeasure;
    @XmlElement(name = "Birthdate")
    private Date birthdate;
    @XmlElement(name = "CitizenshipCountryCode")
    private String citizenshipCountryCode;  //token with value pattern [A-Z]{2}
    @XmlElement(name = "ContactInfoRefID")
    private String contactInfoRefId;
    @XmlElement(name = "Employer")
    private Employer employer;
    @XmlElement(name = "FOID")
    private FOID foid;
    @XmlElement(name = "IdentityDoc")
    private List<IdentityDoc> identityDoc;
    @XmlElement(name = "Individual")
    private Individual individual;
    @XmlElement(name = "LangUsage")
    private List<LangUsage> langUsage;
    @XmlElement(name = "LoyaltyProgramAccount")
    private List<LoyaltyProgramAccount> loyaltyProgramAccount;
    @XmlElement(name = "PaxGroup")
    private PaxGroup paxGroup; //token and cant be null
    @XmlElement(name = "PaxID")
    private String paxId; //token and cant be null
    @XmlElement(name = "PaxRefID")
    private String paxRefId;
    @XmlElement(name = "ProfileConsentInd")
    private Boolean profileConsentInd;
    @XmlElement(name = "ProfileID_Text")
    private String profileIdText;
    @XmlElement(name = "PTC")
    private String ptc; // string with value [A-Z]{3}
    @XmlElement(name = "RedressCase")
    private List<RedressCase> redressCase;
    @XmlElement(name = "Remark")
    private List<Remark> remark;
    @XmlElement(name = "ResidenceCountryCode")
    private String residenceCountryCode; //value [A-Z]{2}
}
