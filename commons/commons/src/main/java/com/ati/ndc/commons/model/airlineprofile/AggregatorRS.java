package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "AggregatorRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class AggregatorRS {
    @XmlElement(name = "AggregatorID")
    private String AggregatorId;    //cant be null
    @XmlElement(name = "ContactInfo")
    private List<ContactInfoRS> contactInfo;
    @XmlElement(name = "Name")
    private String name;
}