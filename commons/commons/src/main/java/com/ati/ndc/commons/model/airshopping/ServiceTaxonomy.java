package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ServiceTaxonomy")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceTaxonomy {
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "ServiceFeature")
    private List<ServiceFeature> serviceFeature;
    @XmlElement(name = "TaxonomyCode")
    private String taxonomyCode;
}
