package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.TypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "TravelAgencyRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class TravelAgencyRS {
    @XmlElement(name = "AgencyID")
    private String agencyId;    //cant be null
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "IATA_Number")
    private Integer iataNumber;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "PseudoCityID")
    private String pseudoCityId;
    @XmlElement(name = "TravelAgent")
    private TravelAgent travelAgent;
    @XmlElement(name = "TypeCode")
    private TypeCode typeCode;
}
