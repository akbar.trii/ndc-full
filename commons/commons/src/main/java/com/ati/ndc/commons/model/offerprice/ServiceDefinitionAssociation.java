package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ServiceDefinitionAssociation")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinitionAssociation {
    //choices
    @XmlElement(name = "BaggageAllowanceRef")
    private BaggageAllowanceRef baggageAllowanceRef;
    @XmlElement(name = "SeatProfileRef")
    private SeatProfileRef seatProfileRef;
    @XmlElement(name = "ServiceBundle")
    private ServiceBundle serviceBundle;
}
