package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.TripPurposeCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ShoppingCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShoppingCriteria {
    @XmlElement(name = "AllianceCriteria")
    private AllianceCriteria allianceCriteria;
    @XmlElement(name = "BaggagePricingCriteria")
    private BaggagePricingCriteria baggagePricingCriteria;
    @XmlElement(name = "CabinTypeCriteria")
    private List<CabinType> cabinTypeCriteria;
    @XmlElement(name = "CarrierCriteria")
    private List<CarrierCriteria> carrierCriteria;
    @XmlElement(name = "ConnectionCriteria")
    private List<ConnectionCriteria> connectionCriteria;
    @XmlElement(name = "ExistingOrderCriteria")
    private ExistingOrderCriteria existingOrderCriteria;
    @XmlElement(name = "FareCriteria")
    private List<FareCriteria> fareCriteria;
    @XmlElement(name = "FlightCriteria")
    private FlightCriteria flightCriteria;
    @XmlElement(name = "OfferItemTypeCriteria")
    private List<OfferItemTypeCriteria> offerItemTypeCriteria;
    @XmlElement(name = "PaymentMethodCriteria")
    private List<PaymentMethodCriteria> paymentMethodCriteria;
    @XmlElement(name = "PricingMethodCriteria")
    private PricingMethodCriteria pricingMethodCriteria;
    @XmlElement(name = "ProgramCriteria")
    private List<ProgramCriteria> programCriteria;
    @XmlElement(name = "PromotionCriteria")
    private PromotionCriteria promotionCriteria;
    @XmlElement(name = "SeatCriteria")
    private List<SeatCriteria> seatCriteria;
    @XmlElement(name = "SegmentDurationCriteria")
    private JourneyDurationCriteria segmentDurationCriteria;
    @XmlElement(name = "ServiceCriteria")
    private ServiceCriteria serviceCriteria;
    @XmlElement(name = "SpecialNeedsCriteria")
    private SpecialService specialNeedsCriteria;
    @XmlElement(name = "TripPurposeCode")
    private TripPurposeCodeType tripPurposeCode;
}