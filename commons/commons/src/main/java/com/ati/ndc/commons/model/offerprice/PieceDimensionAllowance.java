package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PieceDimensionAllowance")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PieceDimensionAllowance {
    @XmlElement(name = "ApplicableBagText")
    private String applicableBagText;
    @XmlElement(name = "ApplicablePartyText")
    private String applicablePartyText;
    @XmlElement(name = "BaggageDimensionCategory")
    private String baggageDimensionCategory;   //cant be null
    @XmlElement(name = "DescText")
    private List<String> descText;  //max 99 elements
    @XmlElement(name = "MaxMeasure")
    private Double maxMeasure; //cant be null
    @XmlElement(name = "MinMeasure")
    private Double minMeasure;
    @XmlElement(name = "Qty")
    private Double qty;
}
