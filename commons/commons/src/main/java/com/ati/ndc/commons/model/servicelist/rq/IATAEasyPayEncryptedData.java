package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "IATAEasyPayEncryptedData")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAEasyPayEncryptedData {
    @XmlElement(name = "EncryptedAccountBinary")
    private String encryptedAccountBinary;
    @XmlElement(name = "KeyNameText")
    private String keyNameText;
}