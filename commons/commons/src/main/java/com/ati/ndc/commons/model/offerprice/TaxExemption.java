package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@XmlRootElement(name = "TaxExemption")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaxExemption {
    @XmlElement(name = "AllRefundableInd")
    private Boolean allRefundableInd;
    @XmlElement(name = "ApproximateInd")
    private Boolean approximateInd;
    @XmlElement(name = "CollectionInd")
    private Boolean collectionInd;
    @XmlElement(name = "GuaranteeTimeLimitDateTime")
    private Date guaranteeTimeLimitDateTime;
    @XmlElement(name = "RefundMethodText")
    private String refundMethodText;
    @XmlElement(name = "Tax")
    private List<Tax> tax;
    @XmlElement(name = "TotalRefundableTaxAmount")
    private Double totalRefundableTaxAmount;
    @XmlElement(name = "TotalTaxAmount")
    private Double totalTaxAmount;
}
