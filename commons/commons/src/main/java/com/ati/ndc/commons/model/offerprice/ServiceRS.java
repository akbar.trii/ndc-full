package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "ServiceRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceRS {
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ServiceDefinitionRefId")
    private String serviceDefinitionRefId;  //cant be null
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "ServiceRefID")
    private String serviceRefId;
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
    @XmlElement(name = "ValidatingCarrier")
    private Carrier validatingCarrier;
}