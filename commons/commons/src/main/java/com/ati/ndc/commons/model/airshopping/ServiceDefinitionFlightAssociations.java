package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ServiceDefinitionFlightAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinitionFlightAssociations {
    //choices
    @XmlElement(name = "DatedOperatingLegRef")
    private DatedOperatingLegRef datedOperatingLegRef;
    @XmlElement(name = "PaxSegmentRef")
    private PaxSegmentRef paxSegmentRef;
}
