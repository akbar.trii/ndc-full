package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxJourneyList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxJourneyListRQ {
    @XmlElement(name = "PaxJourney")
    private List<PaxJourneyRQ> paxJourney;    //cant be null
}
