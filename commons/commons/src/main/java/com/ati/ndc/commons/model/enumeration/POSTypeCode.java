package com.ati.ndc.commons.model.enumeration;

public enum POSTypeCode {
    A,  //Airline Specific Codes
    H,  //Home IATA Agency No.
    I,  //IATA Travel Agency No.
    L,  //LNIATA Number (CRT Address)
    T,  //Pseudo Code/Travel Agency Code
    U,  //Home Travel Agency Code
    V,  //CRS/CXR Department Code
    X   //Department/Identifier
}
