package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareDetail")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareDetail {
    @XmlElement(name = "AccountCode")
    private String accountCode;
    @XmlElement(name = "FareComponent")
    private List<FareComponent> fareComponent;
    @XmlElement(name = "FareIndCode")
    private String fareIndCode;
    @XmlElement(name = "FarePriceType")
    private List<FarePriceType> farePriceType;    //cant be null and max 3 element
    @XmlElement(name = "FareRefText")
    private String fareRefText;
    @XmlElement(name = "FiledFareInd")
    private Boolean filedFareInd;
    @XmlElement(name = "NetReportingCodeText")
    private String netReportingCodeText;
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefID;
    @XmlElement(name = "PricingSystemCodeText")
    private String pricingSystemCodeText;
    @XmlElement(name = "StatisticalCodeText")
    private String statisticalCodeText;
    @XmlElement(name = "TourCode")
    private String tourCode;
}
