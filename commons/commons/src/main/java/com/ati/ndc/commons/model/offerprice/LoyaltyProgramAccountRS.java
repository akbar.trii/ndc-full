package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "LoyaltyProgramAccountRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoyaltyProgramAccountRS {
    @XmlElement(name = "AccountNumber")
    private String accountNumber;
    @XmlElement(name = "Alliance")
    private AllianceRS alliance;
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "ProgramCode")
    private String programCode;
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "ProviderName")
    private String providerName;
    @XmlElement(name = "SignInID")
    private String signInId;
    @XmlElement(name = "TierCode")
    private String tierCode;
    @XmlElement(name = "TierName")
    private String tierName;
    @XmlElement(name = "TierPriorityText")
    private String tierPriorityText;
    @XmlElement(name = "URL")
    private String url;
}