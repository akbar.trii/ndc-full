package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OriginDestList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OriginDestList {
    @XmlElement(name = "OriginDest")
    private List<OriginDest> originDest;  //cant be null
}