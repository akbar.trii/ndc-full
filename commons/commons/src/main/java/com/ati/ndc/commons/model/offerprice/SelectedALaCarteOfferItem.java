package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SelectedALaCarteOfferItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectedALaCarteOfferItem {
    @XmlElement(name = "ALaCarteOfferItemFlightAssociations")
    private AlaCarteOfferItemFlightAssociations aLaCarteOfferItemFlightAssociations;  //cant be null
    @XmlElement(name = "Qty")
    private Double qty; //cant be null
}
