package com.ati.ndc.commons.model.airdocnotif;

import com.ati.ndc.commons.model.enumeration.NameTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "AddlName")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddlName {
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "NameTypeCode")
    private NameTypeCode nameTypeCode;
}