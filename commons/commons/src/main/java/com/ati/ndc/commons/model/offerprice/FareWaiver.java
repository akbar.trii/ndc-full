package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FareWaiver")
@XmlAccessorType(XmlAccessType.FIELD)
@Data 
public class FareWaiver {
    @XmlElement(name = "FareRuleWaiverCode")
    private String fareRuleWaiverCode;  //cant be null
    @XmlElement(name = "FareWaiverTypeCode")
    private String fareWaiverTypeCode;  //cant be null
}