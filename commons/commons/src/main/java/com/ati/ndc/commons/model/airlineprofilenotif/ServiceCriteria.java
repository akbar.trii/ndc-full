package com.ati.ndc.commons.model.airlineprofilenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceCriteria {
    @XmlElement(name = "IncludeInd")
    private Boolean includeInd;
    @XmlElement(name = "RFIC")
    private String rfic;    //with value [0-9A-Z]{1,3}
    @XmlElement(name = "RFISC")
    private List<String> rfisc; //max 99 elements
    @XmlElement(name = "TaxonomyCode")
    private List<String> taxonomyCode;
}
