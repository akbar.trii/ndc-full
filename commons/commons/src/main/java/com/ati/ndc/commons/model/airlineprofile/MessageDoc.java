package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "MessageDoc")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageDoc {
    @XmlElement(name = "Name")
    private String name; //token
    @XmlElement(name = "RefVersionNumber")
    private Double refVersionNumber;
}