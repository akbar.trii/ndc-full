package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.CurAppCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CurParameter")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurParameter {
    @XmlElement(name = "AppCode")
    private CurAppCode appCode;
    @XmlElement(name = "DecimalsAllowedNumber")
    private Integer decimalsAllowedNumber;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "RequestedCurCode")
    private String requestedCurCode;
}