package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BaggageItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaggageItem {
    @XmlElement(name = "BagItemDetails")
    private List<BagItemDetails> bagItemDetails;
    @XmlElement(name = "Price")
    private Price price;
    @XmlElement(name = "ValidatingCarrierCode")
    private String validatingCarrierCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
}
