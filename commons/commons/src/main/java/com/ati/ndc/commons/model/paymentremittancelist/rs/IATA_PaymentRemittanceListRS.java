package com.ati.ndc.commons.model.paymentremittancelist.rs;

import com.ati.ndc.commons.model.Remittance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentRemittanceListRS {
    //choices, must select 1 value of these var
    private List<Error> error;
    private List<Remittance> remittance;
    //end

//    private PayloadStandardAttributes payloadStandardAttributes;
}