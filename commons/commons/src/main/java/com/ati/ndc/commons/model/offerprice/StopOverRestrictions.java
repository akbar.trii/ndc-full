package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "StopOverRestrictions")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class StopOverRestrictions {
    @XmlElement(name = "AvailInd")
    private Boolean availInd;
    @XmlElement(name = "ChargeableInd")
    private Boolean chargeableInd;
    @XmlElement(name = "MaximumStopOversPermittedQty")
    private Double maximumStopOversPermittedQty;
    @XmlElement(name = "PaxRefID")
    private String paxRefId;
    @XmlElement(name = "StopOverLocation")
    private List<StopOverLocation> stopOverLocation;
}
