package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import com.ati.ndc.commons.model.enumeration.StatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OrderItemInvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemInvGuaranteeRQ {
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemID")
    private String orderItemID; //cant be null && token
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;   // e.g RET (Retail), WHO (Wholesale), COR (Corporate)
    @XmlElement(name = "OwnerCode")
    private String ownerCode; //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;    // token
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;   //URI
}
