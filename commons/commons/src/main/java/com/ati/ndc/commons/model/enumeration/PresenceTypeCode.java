package com.ati.ndc.commons.model.enumeration;

public enum PresenceTypeCode {
    CN,     //Customer not present
    CP,     //Customer present -> (e.g. for non-PaymentCard payment methods)
    CPCN,   //Customer present and card not present
    CPCP    //Customer present and card present
}