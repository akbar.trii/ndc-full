package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "FOID")
@XmlAccessorType(XmlAccessType.FIELD)
public class FOID {
    @XmlElement(name = "CreditCardIssuerCode")
    private String creditCardIssuerCode;
    @XmlElement(name = "FOID_ID")
    private String foidId; //cant be null
    @XmlElement(name = "FOID_TypeText")
    private String foidTypeText;   //cant be null
}