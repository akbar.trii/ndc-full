package com.ati.ndc.commons.model.airlineprofilenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "ContactInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactInfo {
    @XmlElement(name = "ContactInfoID")
    private String contactInfoId;
    @XmlElement(name = "ContactPurposeText")
    private List<String> contactPurposeText;    //max 99 element
    @XmlElement(name = "ContactRefusedInd")
    private Boolean contactRefusedInd;
    @XmlElement(name = "EmailAddress")
    private List<EmailAddress> emailAddress;
    @XmlElement(name = "Individual")
    private Individual individual;
    @XmlElement(name = "IndividualRefID")
    private String individualRefId;
    @XmlElement(name = "OtherAddress")
    private List<OtherAddress> otherAddress;
    @XmlElement(name = "PaxSegmentRefID")
    private String paxSegmentRefId;
    @XmlElement(name = "Phone")
    private List<Phone> phone;
    @XmlElement(name = "PostalAddress")
    private List<PostalAddress> postalAddress;
    @XmlElement(name = "RelationshipToPax")
    private String relationshipToPax;
}
