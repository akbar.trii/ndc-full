package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DataLists")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class DataLists {
    @XmlElement(name = "BaggageAllowanceList")
    private BaggageAllowanceList baggageAllowanceList;
    @XmlElement(name = "BaggageDisclosureList")
    private BaggageDisclosureList baggageDisclosureList;
    @XmlElement(name = "ContactInfoList")
    private ContactInfoList contactInfoList;
    @XmlElement(name = "DisclosureList")
    private DisclosureList disclosureList;
    @XmlElement(name = "FareList")
    private FareList fareList;
    @XmlElement(name = "MediaList")
    private MediaList mediaList;
    @XmlElement(name = "OriginDestList")
    private OriginDestList originDestList;
    @XmlElement(name = "PaxJourneyList")
    private PaxJourneyList paxJourneyList;
    @XmlElement(name = "PaxList")
    private PaxList paxList;
    @XmlElement(name = "PaxSegmentList")
    private PaxSegmentList paxSegmentList;
    @XmlElement(name = "PenaltyList")
    private PenaltyList penaltyList;
    @XmlElement(name = "PriceClassList")
    private PriceClassList priceClassList;
    @XmlElement(name = "SeatProfileList")
    private SeatProfileList seatProfileList;
    @XmlElement(name = "ServiceDefinitionList")
    private ServiceDefinitionList serviceDefinitionList;
    @XmlElement(name = "TermsList")
    private TermsList termsList;
}
