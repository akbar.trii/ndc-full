package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CountrySubDivision")
@XmlAccessorType(XmlAccessType.FIELD)
public class CountrySubDivision {
    @XmlElement(name = "Country")
    private Country country;
    @XmlElement(name = "CountrySubDivisionCode")
    private String countrySubDivisionCode;
    @XmlElement(name = "CountrySubDivisionName")
    private String countrySubDivisionName;
}