package com.ati.ndc.commons.model.invguarantee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BookingRef")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingRef {
    @XmlElement(name = "BookingEntity")
    private BookingEntity bookingEntity;
    @XmlElement(name = "BookingRefID")
    private String bookingRefId;    //token with max length 6 and cant be null
    @XmlElement(name = "BookingRefTypeCode")
    private String bookingRefTypeCode;
}