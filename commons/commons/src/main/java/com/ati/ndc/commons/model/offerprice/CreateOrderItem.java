package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "CreateOrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderItem {
    @XmlElement(name = "OfferItemID")
    private String offerItemId; //cant be null
    @XmlElement(name = "OfferItemTimeLimits")
    private List<OfferItemTimeLimits> offerItemTimeLimits;
    @XmlElement(name = "OfferItemtype")
    private OfferItemtype offerItemtype;    //cant be null
    @XmlElement(name = "CreateOwnerCodeOrderItem")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;
}
