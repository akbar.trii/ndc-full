package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemID")
    private String orderItemId;
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;   // e.g RET (Retail), WHO (Wholesale), COR (Corporate)
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "Service")
    private List<String> service;
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;
}
