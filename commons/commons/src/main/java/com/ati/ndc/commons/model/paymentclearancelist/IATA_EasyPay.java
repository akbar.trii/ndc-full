package com.ati.ndc.commons.model.paymentclearancelist;

import com.ati.ndc.commons.model.SettlementData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IATA_EasyPay {
    private String approvalCode;
    private SettlementData settlementData;
}