package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BookingInstructions")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BookingInstructions {
    @XmlElement(name = "MethodText")
    private String methodText;
    @XmlElement(name = "OtherServiceInfoText")
    private List<String> otherServiceInfoText;
    @XmlElement(name = "SpecialService")
    private List<SpecialService> specialService;
    @XmlElement(name = "UpgradeMethodCode")
    private String upgradeMethodCode;
    @XmlElement(name = "UpgradeNewClassCode")
    private String upgradeNewClassCode; //token with length = 1
}
