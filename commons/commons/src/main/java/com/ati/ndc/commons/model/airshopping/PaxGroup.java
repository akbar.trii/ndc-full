package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PaxGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaxGroup {
    @XmlElement(name = "ContactInfo")
    private List<ContactInfo> contactInfo;
    @XmlElement(name = "IntendedPaxQty")
    private Double intendedPaxQty;
    @XmlElement(name = "PaxGroupID")
    private String paxGroupId;
    @XmlElement(name = "PaxGroupName")
    private String paxGroupName;
}