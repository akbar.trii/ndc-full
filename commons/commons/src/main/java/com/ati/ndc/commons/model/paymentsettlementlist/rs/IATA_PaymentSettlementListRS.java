package com.ati.ndc.commons.model.paymentsettlementlist.rs;

import com.ati.ndc.commons.model.SettlementType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentSettlementListRS {
    //choices, must pick one of these
    private List<Error> error;
    private List<SettlementType> settlement;
    //end
//    private PayloadStandardAttributes payloadStandardAttributes;
}

