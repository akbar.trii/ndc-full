package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "AlaCarteOfferItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlaCarteOfferItem {
    @XmlElement(name = "CancelRestrictions")
    private List<CancelRestrictions> cancelRestrictions;
    @XmlElement(name = "ChangeRestrictions")
    private List<ChangeRestrictions> changeRestrictions;
    @XmlElement(name = "Commission")
    private List<Commission> commission;
    @XmlElement(name = "Eligibility")
    private Eligibility eligibility;    //cant be null and EligibilityFlightAssociations cant be null
    @XmlElement(name = "OfferItemID")
    private String offerItemID; //cant be null
    @XmlElement(name = "PaymentTimeLimit")
    private PaymentTimeLimit paymentTimeLimit;
    @XmlElement(name = "PriceGuaranteeTimeLimitDateTime")
    private Date priceGuaranteeTimeLimitDateTime;
    @XmlElement(name = "Service")
    private ServiceRS service;    //cant be null
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "UnitPrice")
    private Price unitPrice;    //cant be null
}
