package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RedressCase")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedressCase {
    @XmlElement(name = "Country")
    private Country country;    //cant be null
    @XmlElement(name = "CountryCode")
    private String countryCode; // token with value [A-Z]{2} and cant be null
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "RedressCaseID")
    private String redressCaseId;   //cant be null
}
