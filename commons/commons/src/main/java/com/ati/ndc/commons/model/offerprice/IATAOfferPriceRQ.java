package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_OfferPriceRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATAOfferPriceRQ {
    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Party")
    private Party party;  //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "POS")
    private POS pos;
    @XmlElement(name = "Request")
    private Request request;    //cant be null

    //lihat xml lagi karna masih ada tambahan
}
