package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Service")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Service {
    @XmlElement(name = "InterlineSettlementInfo")
    private InterlineSettlementInfo interlineSettlementInfo;
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;  //cant be null
    @XmlElement(name = "ServiceAssociations")
    private ServiceAssociations serviceAssociations;   //cant be null
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "ServiceRefID")
    private String serviceRefId;
    @XmlElement(name = "ValidatingCarrierCode")
    private String validatingCarrierCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
}