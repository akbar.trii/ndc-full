package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "MarketingMessage")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class MarketingMessage {
    @XmlElement(name = "Desc")
    private Desc desc;  //cant be null
    @XmlElement(name = "GeneralAssociation")
    private List<GeneralAssociation> generalAssociation;
}
