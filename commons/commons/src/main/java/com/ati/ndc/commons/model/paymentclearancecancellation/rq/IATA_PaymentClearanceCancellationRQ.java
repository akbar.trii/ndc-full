package com.ati.ndc.commons.model.paymentclearancecancellation.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceCancellationRQ {
    private List<String> clearanceId;   //token length value 15 with pattern value ([0-9]{7}[A-Za-z0-9]{8}) and cant be null
//    private PayloadStandardAttributes payloadStandardAttributes;
}
