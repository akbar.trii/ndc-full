package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PricingParameter")
@XmlAccessorType(XmlAccessType.FIELD)
public class PricingParameter {
    @XmlElement(name = "AutoExchInd")
    private Boolean autoExchInd;
    @XmlElement(name = "AwardIncludedInd")
    private Boolean awardIncludedInd;
    @XmlElement(name = "AwardOnlyInd")
    private Boolean awardOnlyInd;
    @XmlElement(name = "OverrideCurCode")
    private String overrideCurCode;
    @XmlElement(name = "SimplePricingInd")
    private Boolean simplePricingInd;
    @XmlElement(name = "TaxExemption")
    private List<TaxExemption> taxExemption;
}