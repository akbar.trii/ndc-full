package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "PaymentRedirectionType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaymentRedirectionType {
    @XmlElement(name = "ExpirationDateTime")
    private Date expirationDateTime;
    @XmlElement(name = "PaymentRedirectionInd")
    private Boolean paymentRedirectionInd;
    @XmlElement(name = "PaymentRedirectionURI")
    private String paymentRedirectionURI;
}
