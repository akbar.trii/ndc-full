package com.ati.ndc.commons.model.enumeration;

public enum SellerFollowupActionCodeType {
    Accept,             //Accept
    Cancel,             //Cancel, with ability to add optional FareWaiver
	Contact_Airline,    //Contact Airline with the ability to include an option URL for the contact details.
	Reshop              //Reshop, with ability to add optional FareWaiver
}
