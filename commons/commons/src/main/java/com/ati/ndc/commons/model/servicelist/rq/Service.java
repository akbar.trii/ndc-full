package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "Service")
@XmlAccessorType(XmlAccessType.FIELD)
public class Service {
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
    @XmlElement(name = "ValidatingCarrier")
    private Carrier validatingCarrier;
}
