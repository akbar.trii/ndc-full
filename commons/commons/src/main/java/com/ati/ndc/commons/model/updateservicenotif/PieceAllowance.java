package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PieceAllowance")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PieceAllowance {
    @XmlElement(name = "ApplicableBagText")
    private String applicableBagText;
    @XmlElement(name = "ApplicablePartyText")
    private String applicablePartyText; //cant be null
    @XmlElement(name = "Desc")
    private List<String> desc;  //max 99 elements
    @XmlElement(name = "PieceDimensionAllowance")
    private List<PieceDimensionAllowance> pieceDimensionAllowance;
    @XmlElement(name = "PieceWeightAllowance")
    private List<WeightAllowance> pieceWeightAllowance;
    @XmlElement(name = "TotalQty")
    private Double totalQty;   //cant be null
    @XmlElement(name = "TypeText")
    private String typeText;
}
