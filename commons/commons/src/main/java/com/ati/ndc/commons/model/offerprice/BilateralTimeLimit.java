package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "BilateralTimeLimit")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BilateralTimeLimit {
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "TimeLimitDateTime")
    private Date timeLimitDateTime;
}
