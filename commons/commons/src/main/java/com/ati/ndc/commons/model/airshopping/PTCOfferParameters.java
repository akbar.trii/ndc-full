package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PTCOfferParameters")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PTCOfferParameters {
	@XmlElement(name = "PricedPaxNumber")
    private Double pricedPaxNumber;
	@XmlElement(name = "PTC_PricedCode")
    private String ptcPricedCode;  //string with pattern value [A-Z]{3}
	@XmlElement(name = "PTC_RequestedCode")
    private String ptcRequestedCode;   //string with pattern value [A-Z]{3}
	@XmlElement(name = "RequestedPaxNumber")
    private Double requestedPaxNumber;
}