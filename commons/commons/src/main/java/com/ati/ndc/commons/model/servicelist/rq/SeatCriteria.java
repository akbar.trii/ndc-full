package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "SeatCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class SeatCriteria {
    @XmlElement(name = "CharacteristicCode")
    private List<String> characteristicCode;  //max 99 elements
    @XmlElement(name = "ColumnID")
    private String columnId;
    //    SA (Seats are available)
    //    WO (Seats waitlist open for segment)
    //    WC (Seats waitlist closed for segment)
    //    OR (Seats on request only to airline)
    @XmlElement(name = "SeatProfile")
    private List<SeatProfile> seatProfile;
}