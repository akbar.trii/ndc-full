package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.MatchTypeCode;
import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "OfferInvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferInvGuaranteeRQ {
    @XmlElement(name = "OfferID")
    private String offerId;
    @XmlElement(name = "OfferItem")
    private List<OfferItemInvGuaranteeRQ> offerItem;  //cant be null
    @XmlElement(name = "OwnerCode")
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "WebAddressURL")
    private String webAddressURL;
}
