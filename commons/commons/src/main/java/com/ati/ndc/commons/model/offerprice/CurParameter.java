package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.CurAppCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CurParameter")
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CurParameter {
    @XmlElement(name = "AppCode")
    private CurAppCode appCode;
    @XmlElement(name = "CurCode")
    private String curCode;
    @XmlElement(name = "DecimalsAllowedNumber")
    private Double decimalsAllowedNumber;
    @XmlElement(name = "Name")
    private String name;
}
