package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentCard {
    private String cardBrandCode;   //cant be null
    private String cardHolderName;
    private String cardIssuingCountryCode;   //token with value pattern [A-Z]{2}
    private String cardNumber;
    private String cardProductTypeCode; //token
    private String cardSecurityCode; //token
    private Integer cardSeqNumber;
    private String cardTypeText;
//    private CryptographyKey cryptographyKey;
    private String expirationDate;  //pattern value (0[1-9]|1[0-2])[0-9][0-9]
    private String last4DigitsText; //pattern value="[0-9]{4}
    private String protectedCardId; //token
    private String reconciliationId;    //token
    private SecurePaymentAuthenticationInstructionsVersion1 securePaymentAuthenticationInstructionsVersion1;
    private SecurePaymentAuthenticationInstructionsVersion2 securePaymentAuthenticationInstructionsVersion2;
    private SecureProgram secureProgram;
    private Boolean sellerOwnCardInd;
    private Boolean verificationInd;
}