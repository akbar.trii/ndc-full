package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "FlightCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class FlightCriteria {
    @XmlElement(name = "Aircraft")
    private List<Aircraft> aircraft;
    @XmlElement(name = "CabinType")
    private List<CabinType> cabinType;
    @XmlElement(name = "FlightCharacteristicsCriteria")
    private List<FlightCharacteristicsCriteria> flightCharacteristicsCriteria;
    @XmlElement(name = "IATA_AircraftType")
    private List<IATAAircraftType> iataAircraftType;
    @XmlElement(name = "RBD")
    private List<RBD> rbd;
    @XmlElement(name = "WaitListCriteria")
    private List<PrefLevel> waitListCriteria;
}