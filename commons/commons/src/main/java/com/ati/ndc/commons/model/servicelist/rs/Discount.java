package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Discount")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Discount {
    @XmlElement(name = "AppText")
    private String appText;
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "DiscountAmount")
    private Double discountAmount;
    @XmlElement(name = "DiscountPercent")
    private Integer discountPercent;
    @XmlElement(name = "PreDiscountedAmount")
    private Double preDiscountedAmount;
}