package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "FlightRequestType")
@XmlAccessorType(XmlAccessType.FIELD)
public class FlightRequestType {
    //choices
    @XmlElement(name = "AffinityShoppingCriteria")
    private AffinityShoppingCriteria affinityShoppingCriteria;
    @XmlElement(name = "OriginDestCriteria")
    private List<OriginDestCriteria> originDestCriteria;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponseRQ shoppingResponse;
    @XmlElement(name = "SpecificOriginDestCriteria")
    private List<SpecificOriginDestCriteria> specificOriginDestCriteria;
}
