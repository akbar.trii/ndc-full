package com.ati.ndc.commons.model.invguarantee;

import com.ati.ndc.commons.model.enumeration.TripPurposeCodeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Qualifiers")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Qualifiers {
    @XmlElement(name = "LocationCode")
    private List<String> locationCode;  //token && length = 3
    @XmlElement(name = "OfferItemTypeCriteria")
    private List<OfferItemTypeCriteria> offerItemTypeCriteria;
    @XmlElement(name = "ProgramCriteria")
    private List<ProgramCriteria> programCriteria;
    @XmlElement(name = "PromotionCriteria")
    private PromotionCriteria promotionCriteria;
    @XmlElement(name = "SeatCriteria")
    private List<Seat> seatCriteria;
    @XmlElement(name = "ServiceCriteria")
    private ServiceCriteria serviceCriteria;
    @XmlElement(name = "SpecialNeedsCriteria")
    private SpecialService specialNeedsCriteria;
    @XmlElement(name = "TripPurposeCode")
    private TripPurposeCodeType tripPurposeCode;
}
