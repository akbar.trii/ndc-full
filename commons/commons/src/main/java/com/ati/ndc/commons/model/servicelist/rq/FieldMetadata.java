package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FieldMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldMetadata {
    @XmlElement(name = "MandatoryInd")
    private Boolean mandatoryInd;
    @XmlElement(name = "PathText")
    private String pathText;
}