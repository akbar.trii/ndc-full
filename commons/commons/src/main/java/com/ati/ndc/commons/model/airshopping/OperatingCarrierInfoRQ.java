package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "OperatingCarrierInfoRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class OperatingCarrierInfoRQ {
    @XmlElement(name = "CarrierDesigCode")
    private String carrierDesigCode;    //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "CarrierName")
    private String carrierName; //token with patter value ="[A-Z]{2}"
    @XmlElement(name = "OperatingCarrierFlightNumberText")
    private String operatingCarrierFlightNumberText;
    @XmlElement(name = "OperationalSuffixText")
    private String operationalSuffixText;
    @XmlElement(name = "RBD_Code")
    private String rbdCode;
}
