package com.ati.ndc.commons.model.airlineprofile;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Participant")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participant {
    @XmlElement(name = "Aggregator")
    private AggregatorRQ aggregator;
    @XmlElement(name = "Corporation")
    private Corporation corporation;
    @XmlElement(name = "EnabledSystem")
    private EnabledSystem enabledSystem;
    @XmlElement(name = "MarketingCarrier")
    private CarrierRQ marketingCarrier;
    @XmlElement(name = "OperatingCarrier")
    private CarrierRQ operatingCarrier;
    @XmlElement(name = "ORA")
    private CarrierRQ ora;
    @XmlElement(name = "POA")
    private CarrierRQ poa;
    @XmlElement(name = "RetailPartner")
    private RetailPartner retailPartner;
    @XmlElement(name = "TravelAgency")
    private TravelAgency travelAgency;
}