package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Error")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Error {
    @XmlElement(name = "Code")
    private String code;
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "ErrorID")
    private String errorId;
    @XmlElement(name = "LangCode")
    private String langCode;
    @XmlElement(name = "OwnerName")
    private String ownerName;
    @XmlElement(name = "StatusText")
    private String statusText;
    @XmlElement(name = "TagText")
    private String tagText;
    @XmlElement(name = "TypeCode")
    private String typeCode;
    @XmlElement(name = "URL")
    private String url;
}