package com.ati.ndc.commons.model.enumeration;

public enum MatchTypeCode {
    Full,
    None,
    Other,
    Partial
}
