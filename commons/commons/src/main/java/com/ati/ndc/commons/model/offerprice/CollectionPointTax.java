package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CollectionPointTax")
@XmlAccessorType(XmlAccessType.FIELD)
public class CollectionPointTax {
    @XmlElement(name = "AirportAmount")
    private Double airportAmount;   //cant be null
    @XmlElement(name = "AirportCur")
    private String airportCur;
    @XmlElement(name = "Station")
    private Station station;    //cant be null
}
