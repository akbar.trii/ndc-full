package com.ati.ndc.commons.model.updateservicenotif;

import com.ati.ndc.commons.model.enumeration.BagRuleCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BDC")
@XmlAccessorType(XmlAccessType.FIELD)
public class BDC {
    @XmlElement(name = "BagRuleCode")
    private BagRuleCode bagRuleCode;
    @XmlElement(name = "BDC_AnalysisResultCode")
    private String bdcAnalysisResultCode;
    @XmlElement(name = "BDC_ReasonText")
    private String bdcReasonText;
    @XmlElement(name = "CarrierDesigCode")
    private String carrierDesigCode;    //token with pattern value="([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])" and cant be null
    @XmlElement(name = "CarrierName")
    private String carrierName;
}
