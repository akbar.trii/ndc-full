package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "SpecialService")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecialService {
    @XmlElement(name = "AddlDataMeasure")
    private Double addlDataMeasure;
    @XmlElement(name = "FreeText")
    private String freeText;
    @XmlElement(name = "Qty")
    private Double qty;
    @XmlElement(name = "SpecialServiceCode")
    private String specialServiceCode;
}