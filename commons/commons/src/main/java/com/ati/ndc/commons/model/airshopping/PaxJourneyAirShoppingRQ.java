package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PaxJourneyAirShoppingRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaxJourneyAirShoppingRQ {
    @XmlElement(name = "DistanceMeasure")
    private Double distanceMeasure;
    @XmlElement(name = "Duration")
    private String duration;
    @XmlElement(name = "PaxJourneyID")
    private String paxJourneyId;
    @XmlElement(name = "PaxSegment")
    private List<PaxSegmentRQ> paxSegment;   //cant be null
}