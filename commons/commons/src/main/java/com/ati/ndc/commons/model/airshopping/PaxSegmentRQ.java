package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PaxSegmentRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaxSegmentRQ {
    @XmlElement(name = "Arrival")
    private Arrival arrival;    //cant be null
    @XmlElement(name = "Dep")
    private Arrival Dep;    //cant be null
    @XmlElement(name = "MarketingCarrierInfo")
    private MarketingCarrierInfo marketingCarrierInfo;  //cant be null
    @XmlElement(name = "MarketingCarrierRBD_Code")
    private String marketingCarrierRBDCode;
    @XmlElement(name = "OperatingCarrierInfo")
    private OperatingCarrierInfoRQ operatingCarrierInfo;
    @XmlElement(name = "OperatingCarrierRBD_Code")
    private String operatingCarrierRBDCode;
    @XmlElement(name = "PaxSegmentID")
    private String paxSegmentId;
}
