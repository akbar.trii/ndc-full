package com.ati.ndc.commons.model.airlineprofile;

import com.ati.ndc.commons.model.enumeration.POSTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "POSFilterCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class POSFilterCriteria {
    @XmlElement(name = "Aggregator")
    private AggregatorRS aggregator;
    @XmlElement(name = "POS_CodeText")
    private String posCodeText; //cant be null
    @XmlElement(name = "POS_TypeCode")
    private POSTypeCode posTypeCode;    //cant be null
    @XmlElement(name = "TravelAgencyInd")
    private boolean travelAgencyInd;
}