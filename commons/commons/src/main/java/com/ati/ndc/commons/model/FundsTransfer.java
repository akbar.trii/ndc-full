package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundsTransfer {
    private String accountCode; //token length 4 with pattern ([A-Za-z0-9]{4})
    private String methodCode;  //token length 7 with pattern [A-Z]{7} and cant be null
    private String typeCode;    //token length 3 with pattern [A-Z]{3} and cant be null
    private Date valueDate; //cant be null
}
