package com.ati.ndc.commons.model.servicelist.rs;

import com.ati.ndc.commons.model.enumeration.LevelTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Refund")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Refund {
    @XmlElement(name = "DescText")
    private String descText;    //cant be null
    @XmlElement(name = "FixedAmount")
    private Double fixedAmount;
    @XmlElement(name = "LevelTypeCode")
    private LevelTypeCode levelTypeCode;
    @XmlElement(name = "MaximumAmount")
    private Double maximumAmount;
    @XmlElement(name = "MinimumAmount")
    private Double minimumAmount;
    @XmlElement(name = "PaymentTypeCode")
    private String paymentTypeCode;
    @XmlElement(name = "Percent")
    private Double percent;
}
