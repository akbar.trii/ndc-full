package com.ati.ndc.commons.model.orderreshop.rq;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_OrderReshopRQ extends IATA_OrderChangeRQ {
}