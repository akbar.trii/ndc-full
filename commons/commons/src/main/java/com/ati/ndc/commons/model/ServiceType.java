package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "ServiceType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceType {
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ServiceDefinitionRefId")
    private String serviceDefinitionRefId;  //cant be null
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "ServiceRefID")
    private String serviceRefId;
//    @XmlElement(name = "ServiceTaxonomy")
//    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
    @XmlElement(name = "ValidatingCarrierCode")
    private String validatingCarrierCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
//    @XmlElement(name = "ValidatingCarrier")
//    private Carrier validatingCarrier;
}