package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceDefinitionRef")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinitionRef {
    @XmlElement(name = "PaxSegmentRefID")
    private List<String> paxSegmentRefId;
    @XmlElement(name = "ServiceDefinitionFlightAssociations")
    private ServiceDefinitionFlightAssociations serviceDefinitionFlightAssociations;    //cant be null
    @XmlElement(name = "ServiceDefinitionRefID")
    private String serviceDefinitionRefId;  //cant be null
}