package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "SeatProfileRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class SeatProfileRQ {
    @XmlElement(name = "CharacteristicCode")
    private List<String> characteristicCode;  //max 99 elements
}
