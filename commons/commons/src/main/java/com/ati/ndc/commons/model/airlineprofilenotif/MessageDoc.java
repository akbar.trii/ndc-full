package com.ati.ndc.commons.model.airlineprofilenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MessageDoc")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDoc {
    @XmlElement(name = "Name")
    private String name;    //cant be null
    @XmlElement(name = "RefVersionNumber")
    private Double refVersionNumber;    //cant be null
}