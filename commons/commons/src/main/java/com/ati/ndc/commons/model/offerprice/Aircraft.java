package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Aircraft")
@XmlAccessorType(XmlAccessType.FIELD)
public class Aircraft {
    @XmlElement(name = "AircraftRegistrationID")
    private String aircraftRegistrationId;
    @XmlElement(name = "TailNumberText")
    private String tailNumberText;
}
