package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "CarrierOffers")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class CarrierOffers {
    @XmlElement(name = "ALaCarteOffer")
    private List<Offer1> aLaCarteOffer;
    @XmlElement(name = "CarrierOffersSummary")
    private AllOffersSummary carrierOffersSummary;
    @XmlElement(name = "Offer")
    private List<Offer> offer;
    @XmlElement(name = "PriceCalendar")
    private List<PriceCalendar> priceCalendar;
}
