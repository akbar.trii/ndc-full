package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "LangUsage")
@XmlAccessorType(XmlAccessType.FIELD)
public class LangUsage {
    @XmlElement(name = "LangCode")
    private String langCode;
    @XmlElement(name = "LangUsageText")
    private String langUsageText;
}