package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "AffinityShoppingCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class AffinityShoppingCriteria {
    @XmlElement(name = "AffinityOriginDest")
    private List<AffinityOriginDest> affinityOriginDest;    //cant be null
    @XmlElement(name = "BudgetAmount")
    private Double budgetAmount;
    @XmlElement(name = "JourneyDistanceCriteria")
    private JourneyDistanceCriteria journeyDistanceCriteria;
    @XmlElement(name = "JourneyDurationCriteria")
    private JourneyDurationCriteria journeyDurationCriteria;
    @XmlElement(name = "KeywordPref")
    private List<KeywordPref> keywordPref;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponseRQ shoppingResponse;
    @XmlElement(name = "StayPeriod")
    private StayPeriod stayPeriod;
}