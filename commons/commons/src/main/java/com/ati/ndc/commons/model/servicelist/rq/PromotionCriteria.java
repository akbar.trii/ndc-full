package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PromotionCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromotionCriteria {
    @XmlElement(name = "OwnerName")
    private String ownerName;
    @XmlElement(name = "PromotionID")
    private String promotionId; //cant be null
    @XmlElement(name = "PromotionIssuer")
    private PromotionIssuer promotionIssuer;
    @XmlElement(name = "Remark")
    private List<Remark> remark;
    @XmlElement(name = "URL")
    private String url;
}