package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.BaggageTypeCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "BaggagePricingCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaggagePricingCriteria {
    @XmlElement(name = "BaggageOptionCode")
    private List<BaggageTypeCodeType> baggageOptionCode;  //max 2 element
    @XmlElement(name = "CommercialAgreementID")
    private String commercialAgreementId;
    @XmlElement(name = "DeferralInd")
    private Boolean deferralInd;
    @XmlElement(name = "FixedPrepaidInd")
    private Boolean fixedPrepaidInd;
    @XmlElement(name = "IncludeSettlementInd")
    private Boolean includeSettlementInd;
    @XmlElement(name = "OptionalChargesCode")
    private String optionalChargesCode;
    @XmlElement(name = "RequestedActionCode")
    private String requestedActionCode;
}