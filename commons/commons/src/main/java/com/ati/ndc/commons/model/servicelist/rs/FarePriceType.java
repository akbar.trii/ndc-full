package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FarePriceType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FarePriceType {
    @XmlElement(name = "FarePriceTypeCode")
    private String farePriceTypeCode;   //cant be null
    @XmlElement(name = "Price")
    private Price price;    //cant be null
}
