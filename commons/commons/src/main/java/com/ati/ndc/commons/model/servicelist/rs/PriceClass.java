package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PriceClass")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PriceClass {
    @XmlElement(name = "CabinType")
    private List<CabinType> cabinType;
    @XmlElement(name = "Code")
    private String code;
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "DisplayOrderText")
    private String displayOrderText;
    @XmlElement(name = "FareBasisAppText")
    private String fareBasisAppText;
    @XmlElement(name = "FareBasisCode")
    private String fareBasisCode;
    @XmlElement(name = "Name")
    private String name;    //cant be null
    @XmlElement(name = "PriceClassID")
    private String priceClassId;
}
