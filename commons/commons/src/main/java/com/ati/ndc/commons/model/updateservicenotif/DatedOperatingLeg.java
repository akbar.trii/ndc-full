package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DatedOperatingLeg")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class DatedOperatingLeg {
    @XmlElement(name = "Arrival")
    private Arrival arrival;    //cant be null
    @XmlElement(name = "CarrierAircraftType")
    private CarrierAircraftType carrierAircraftType;
    @XmlElement(name = "ChangeofGaugeInd")
    private Boolean changeofGaugeInd;
    @XmlElement(name = "DatedOperatingLegID")
    private String datedOperatingLegId;
    @XmlElement(name = "Dep")
    private Arrival dep;    //cant be null
    @XmlElement(name = "DistanceMeasure")
    private Double distanceMeasure;
    @XmlElement(name = "IATA_AircraftType")
    private IATAAircraftType iataAircraftType;
    @XmlElement(name = "OnGroundDuration")
    private String onGroundDuration;
}