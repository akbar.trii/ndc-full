package com.ati.ndc.commons.model.enumeration;

public enum FlightCharacteristicCodeType {
    AirportChange,
    Direct,
    NonStop,
    OvernightStop,
    RedEye,
    WaitList
}
