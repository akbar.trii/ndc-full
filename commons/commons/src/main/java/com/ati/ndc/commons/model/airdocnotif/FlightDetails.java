package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FlightDetails")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightDetails {
    @XmlElement(name = "Arrival")
    private Arrival arrival;
    @XmlElement(name = "CabinType")
    private CabinType cabinType;
    @XmlElement(name = "Dep")
    private Arrival dep; //cant be null
    @XmlElement(name = "MarketingCarrierFlightNumberText")
    private String marketingCarrierFlightNumberText; //cant be null
    @XmlElement(name = "MarketingCarrierRBD_Code")
    private String marketingCarrierRBDCode; //token
    @XmlElement(name = "OperatingCarrierRBD_Code")
    private String operatingCarrierRBDCode; //token
}