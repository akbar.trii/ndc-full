package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MediaObject")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MediaObject {
    @XmlElement(name = "BinaryObject")
    private byte[] binaryObject;   //base64Binary
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "FileSizeMeasure")
    private Double fileSizeMeasure;
    @XmlElement(name = "HeightMeasure")
    private Double heightMeasure;
    @XmlElement(name = "HintText")
    private String hintText;
    @XmlElement(name = "MediaID")
    private String mediaId;
    @XmlElement(name = "RenderingInstructionsText")
    private String renderingInstructionsText;
    @XmlElement(name = "RenderingMethodText")
    private String renderingMethodText;
    @XmlElement(name = "RenderingOutputFormatText")
    private String renderingOutputFormatText;
    @XmlElement(name = "RenderingOverviewText")
    private String renderingOverviewText;
    @XmlElement(name = "WidthMeasure")
    private Double widthMeasure;
}