package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "InventoryGuarantee")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryGuarantee {
    @XmlElement(name = "Amount")
    private Double amount;
    @XmlElement(name = "Associations")
    private Associations associations;
    @XmlElement(name = "InventoryGuaranteeID")
    private String inventoryGuaranteeId;    //cant be null
    @XmlElement(name = "InventoryGuaranteeTimeLimitDateTime")
    private Date inventoryGuaranteeTimeLimitDateTime;   //cant be null
    @XmlElement(name = "NoGuaranteeInd")
    private Boolean noGuaranteeInd;
    @XmlElement(name = "WaitListInd")
    private Boolean waitListInd;
}
