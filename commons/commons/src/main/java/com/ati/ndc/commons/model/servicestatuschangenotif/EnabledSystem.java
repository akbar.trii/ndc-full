package com.ati.ndc.commons.model.servicestatuschangenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "EnabledSystem")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnabledSystem {
    @XmlElement(name = "ContactInfo")
    private List<ContactInfo> contactInfo;
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "SystemID")
    private String systemId;   //cant be null
}