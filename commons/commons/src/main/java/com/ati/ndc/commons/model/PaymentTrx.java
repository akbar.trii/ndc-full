package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTrx {
    private String descText;
    private Device device;
    private String inputCurCode;
    private Boolean maximumTryInd;
    private Boolean notProcessedInd;
    private String originalId;  //token
    private PaymentAddressVerification paymentAddressVerification;
    private Boolean retryInd;
    private String settlementCurCode;
    private Date timestampDateTime;
    private List<String> trxDataText;
    private String trxId;   //token
    private String trxTypeText;
}