package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettlementType {
    private List<ClearanceType> clearance;  //cant be null
    private Integer clearanceCount; //cant be null
    private Date date;  //cant be null
    private FundsTransfer fundsTransfer;    //cant be null
    private List<String> settleId;  //token length 10 with pattern (SWS|BIL)[A-Za-z0-9]{7} and cant be null
//    private PromotionIssuer SettlementPayee;    //cant be null
    private Double totalAmount;  //cant be null
}