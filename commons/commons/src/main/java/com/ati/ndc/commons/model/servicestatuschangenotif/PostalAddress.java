package com.ati.ndc.commons.model.servicestatuschangenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PostalAddress")
@XmlAccessorType(XmlAccessType.FIELD)
public class PostalAddress {
    @XmlElement(name = "BuildingRoomText")
    private String buildingRoomText;
    @XmlElement(name = "CityName")
    private String cityName;
    @XmlElement(name = "ContactTypeText")
    private String contactTypeText;
    @XmlElement(name = "CountryCode")
    private String countryCode;    //token with value pattern [A-Z]{2}
    @XmlElement(name = "CountryName")
    private String countryName;
    @XmlElement(name = "CountrySubDivisionName")
    private String countrySubDivisionName;
    @XmlElement(name = "PO_BoxCode")
    private String poBoxCode;
    @XmlElement(name = "PostalCode")
    private String postalCode;
    @XmlElement(name = "StreetText")
    private List<String> streetText;    //max value 2
}