package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
    @XmlElement(name = "Commission")
    private List<Commission> commission;
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;
    @XmlElement(name = "PaxGroup")
    private PaxGroup paxGroup;
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "SellerFollowUpAction")
    private SellerFollowUpAction sellerFollowUpAction;
    @XmlElement(name = "Service")
    private List<ServiceRQ> service;  //cant be null
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
}
