package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PaxGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaxGroup {
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;  //max 99
    @XmlElement(name = "IntendedPaxQty")
    private Double intendedPaxQty;
    @XmlElement(name = "PaxGroupID")
    private String paxGroupId;
    @XmlElement(name = "PaxGroupName")
    private String paxGroupName;
}
