package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.enumeration.StatusCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemID")
    private String orderItemId;
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;
    @XmlElement(name = "OwnerCode")
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private String ownerTypeCode;
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "Service")
    private List<Service> service;  //cant be null
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "StatusCode")
    private StatusCode statusCode;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;
}