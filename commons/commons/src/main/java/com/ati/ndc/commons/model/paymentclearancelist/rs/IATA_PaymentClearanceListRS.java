package com.ati.ndc.commons.model.paymentclearancelist.rs;

import com.ati.ndc.commons.model.ClearanceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceListRS {
    // choice
    private List<ClearanceType> clearance;
    private List<Error> error;
    //end
//    private PayloadStandardAttributes payloadStandardAttributes;
}