package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Remittance {
    private List<ClearanceType> clearance;  //cant be null
    private Integer clearanceCount; //cant be null
    private FundsTransfer fundsTransfer;    //cant be null
    private String remitId; //token length 10 with pattern (SWR|BIL)[A-Za-z0-9]{7} and cant be null
//    private PromotionIssuer remittancePayer;    //cant be null
    private Double totalAmount; //cant be null
}