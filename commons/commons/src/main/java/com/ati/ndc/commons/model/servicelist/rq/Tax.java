package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.enumeration.TaxTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "Tax")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tax {
    @XmlElement(name = "AddlFiledTaxCode")
    private String addlFiledTaxCode;
    @XmlElement(name = "AddlTaxCode")
    private String addlTaxCode;
    @XmlElement(name = "Amount")
    private Double amount;  //cant be null
    @XmlElement(name = "ApproximateInd")
    private Boolean approximateInd;
    @XmlElement(name = "AppTypeCode")
    private String appTypeCode;
    @XmlElement(name = "CollectionInd")
    private Boolean collectionInd;
    @XmlElement(name = "CollectionPointTax")
    private List<CollectionPointTax> collectionPointTax;
    @XmlElement(name = "Country")
    private Country country;
    @XmlElement(name = "CountrySubDivision")
    private CountrySubDivision countrySubDivision;
    @XmlElement(name = "CurConversion")
    private CurConversion curConversion;
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "FiledAmount")
    private Double filedAmount;
    @XmlElement(name = "FiledTaxCode")
    private String filedTaxCode;
    @XmlElement(name = "QualifierCode")
    private String qualifierCode;
    @XmlElement(name = "RefundInd")
    private Boolean refundInd;
    @XmlElement(name = "TaxCode")
    private String taxCode;
    @XmlElement(name = "TaxName")
    private String taxName;
    @XmlElement(name = "TaxTypeCode")
    private TaxTypeCode taxTypeCode;
}