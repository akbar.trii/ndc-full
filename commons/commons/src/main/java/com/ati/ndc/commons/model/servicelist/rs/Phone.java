package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Phone")
@XmlAccessorType(XmlAccessType.FIELD)
public class Phone {
    @XmlElement(name = "AreaCodeNumber")
    private String areaCodeNumber;
    @XmlElement(name = "ContactTypeText")
    private String contactTypeText;
    @XmlElement(name = "CountryDialingCode")
    private String countryDialingCode;
    @XmlElement(name = "ExtensionNumber")
    private String extensionNumber;
    @XmlElement(name = "PhoneNumber")
    private String phoneNumber;
}