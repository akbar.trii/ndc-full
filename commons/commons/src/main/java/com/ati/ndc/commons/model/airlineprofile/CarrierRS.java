package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CarrierRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class CarrierRS {
    @XmlElement(name = "AirlineDesigCode")
    private String airlineDesigCode; //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "ContactInfo")
    private List<ContactInfoRS> contactInfo;
    @XmlElement(name = "DuplicateDesigInd")
    private Boolean duplicateDesigInd;
    @XmlElement(name = "Name")
    private String name;
}