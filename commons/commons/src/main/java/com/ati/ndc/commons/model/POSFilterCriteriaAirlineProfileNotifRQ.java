package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.POSTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "POS_FilterCriteriaAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class POSFilterCriteriaAirlineProfileNotifRQ {
    @XmlElement(name = "Aggregator")
    private AggregatorAirlineProfileNotifRQ aggregator;
    @XmlElement(name = "POS_CodeText")
    private String posCodeText; //cant be null
    @XmlElement(name = "POS_TypeCode")
    private POSTypeCode posTypeCode;    //cant be null
    @XmlElement(name = "TravelAgencyInd")
    private boolean travelAgencyInd;
}