package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    @XmlElement(name = "AirlineProfile")
    List<AirlineProfile> airlineProfile;    //cant be null
//    @XmlElement(name = "SupportedMessageInfo")
//    List<SupportedMessageInfo> supportedMessageInfo;
//    @XmlElement(name = "Warning")
//    List<Warning> warning;
}