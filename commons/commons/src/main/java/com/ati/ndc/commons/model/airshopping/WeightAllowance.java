package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "WeightAllowance")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class WeightAllowance {
    @XmlElement(name = "ApplicableBagText")
    private String applicableBagText;
    @XmlElement(name = "ApplicablePartyText")
    private String applicablePartyText;
    @XmlElement(name = "DescText")
    private List<String> descText;  //max 99 elements
    @XmlElement(name = "MaximumWeightMeasure")
    private Double maximumWeightMeasure;    //cant be null
}
