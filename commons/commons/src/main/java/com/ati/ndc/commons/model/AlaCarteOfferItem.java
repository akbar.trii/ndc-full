package com.ati.ndc.commons.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class AlaCarteOfferItem {
//    private List<CancelRestrictions> cancelRestrictions;
//    private List<ChangeRestrictions> changeRestrictions;
//    private List<Commission> commission;
//    private Eligibility eligibility;    //cant be null
    private String offerItemID; //cant be null
//    private PaymentTimeLimit paymentTimeLimit;
    private Date priceGuaranteeTimeLimitDateTime;
    private ServiceType service;    //cant be null
//    private List<ServiceTaxonomy> serviceTaxonomy;
    private PriceType unitPrice;    //cant be null
}
