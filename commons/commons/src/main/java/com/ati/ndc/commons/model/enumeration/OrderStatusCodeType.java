package com.ati.ndc.commons.model.enumeration;

public enum OrderStatusCodeType {
    CLOSED,     //Once all Order Items are FULLY PAID and Services are either DELIVERED or REFUNDED or EXPIRED
    FROZEN,     //emergency lock
	OPENED      //Order was built based on offer information
}
