package com.ati.ndc.commons.model.updateservicenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Aggregator")
@XmlAccessorType(XmlAccessType.FIELD)
public class Aggregator {
    @XmlElement(name = "AggregatorID")
    private String AggregatorId;    //cant be null
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "Name")
    private String name;
}