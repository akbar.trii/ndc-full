package com.ati.ndc.commons.model;

import lombok.Data;

import java.util.List;

@Data
public class BaggageFlightAssociations {
    private List<String> datedOperatingLegRefId;  //cant be null
    private List<String> paxSegmentRefId; //cant be null
}
