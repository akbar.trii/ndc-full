package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "JourneyPriceClass")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class JourneyPriceClass {
    @XmlElement(name = "PaxJourneyRefID")
    private String paxJourneyRefId; //cant be null
    @XmlElement(name = "PriceClassRefID")
    private String priceClassRefId;
}
