package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Org")
@XmlAccessorType(XmlAccessType.FIELD)
public class Org {
    @XmlElement(name = "ContactInfo")
    private ContactInfo ContactInfo; //cant be null
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "OrgID")
    private String orgId;    //cant be null
}
