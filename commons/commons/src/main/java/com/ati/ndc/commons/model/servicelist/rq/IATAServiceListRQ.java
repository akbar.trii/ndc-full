package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "IATA_ServiceListRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAServiceListRQ {
    // one of these element shud has value (choice)
    @XmlElement(name = "AugmentationPoint")
    private String augmentationPoint;   //value is any element, still wrong datatype
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Party")
    private Party party;  //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "POS")
    private POS pos;
    @XmlElement(name = "Request")
    private Request request;    //cant be null
}