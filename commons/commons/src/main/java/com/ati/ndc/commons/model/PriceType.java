package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.List;

@Data
public class PriceType {
    private Double baseAmount;
    private Date baseAmountGuaranteeTimeLimitDateTime;
    private Double equivAmount;
    private Double loyaltyUnitAmount;
    private String loyaltyUnitName;
    private Boolean maskedInd;
    private Double totalAmount;
}
