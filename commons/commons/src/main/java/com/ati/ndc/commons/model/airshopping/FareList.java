package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareList {
    @XmlElement(name = "FareGroup")
    private List<FareGroup> fareGroup;    //cant be null
}