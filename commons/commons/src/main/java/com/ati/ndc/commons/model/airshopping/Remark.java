package com.ati.ndc.commons.model.airshopping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Remark")
@XmlAccessorType(XmlAccessType.FIELD)
public class Remark {
    @XmlElement(name = "DisplayInd")
    private Boolean displayInd;
    @XmlElement(name = "RemarkText")
    private String remarkText;
    @XmlElement(name = "Timestamp")
    private Timestamp timestamp;
}