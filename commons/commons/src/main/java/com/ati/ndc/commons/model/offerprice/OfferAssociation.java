package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OfferAssociation")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OfferAssociation {
    @XmlElement(name = "OfferItemRefID")
    private List<String> offerItemRefId;
    @XmlElement(name = "OfferRefID")
    private String offerRefId;  //cant be null
}