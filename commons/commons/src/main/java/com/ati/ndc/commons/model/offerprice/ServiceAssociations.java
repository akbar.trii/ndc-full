package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ServiceAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceAssociations {
    //choices
    @XmlElement(name = "PaxJourneyRef")
    private PaxJourneyRef paxJourneyRef;
    @XmlElement(name = "SeatAssignment")
    private SeatAssignment seatAssignment;
    @XmlElement(name = "ServiceDefinitionRef")
    private ServiceDefinitionRef serviceDefinitionRef;
}
