package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ProgramCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProgramCriteria {
    @XmlElement(name = "CompanyIndexText")
    private String companyIndexText;
    @XmlElement(name = "MemberStatusText")
    private String memberStatusText;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "ProgamContract")
    private ProgamContract progamContract;  //for this, the ID can be null
    @XmlElement(name = "ProgramAccount")
    private List<ProgramAccount> programAccount;    //for this, the ID can be null
    @XmlElement(name = "TypeCode")
    private String typeCode;
}