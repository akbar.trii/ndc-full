package com.ati.ndc.commons.model.updateservicenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RedressCase")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedressCase {
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "RedressCaseID")
    private String redressCaseId;   //cant be null
}
