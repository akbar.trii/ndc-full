package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.PenaltyTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PenaltyRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PenaltyRQ {
    @XmlElement(name = "AppCode")
    private String appCode; //e.g ADE (After departure), NOS (No show), PDE (Prior to departure)
    @XmlElement(name = "CancelFeeInd")
    private Boolean cancelFeeInd;
    @XmlElement(name = "ChangeFeeInd")
    private Boolean changeFeeInd;
    @XmlElement(name = "DescText")
    private List<String> descText;  //max 99
    @XmlElement(name = "FareDetail")
    private FareDetailRQ fareDetail;
    @XmlElement(name = "NetInd")
    private Boolean netInd;
    @XmlElement(name = "PenaltyID")
    private String penaltyId;
    @XmlElement(name = "PenaltyPercent")
    private Double penaltyPercent;
    @XmlElement(name = "Price")
    private PriceType1 price;
    @XmlElement(name = "TypeCode")
    private PenaltyTypeCode typeCode;
    @XmlElement(name = "UpgradeFeeInd")
    private Boolean upgradeFeeInd;
}
