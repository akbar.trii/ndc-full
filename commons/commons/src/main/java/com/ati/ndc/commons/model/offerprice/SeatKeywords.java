package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SeatKeywords")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class SeatKeywords {
    @XmlElement(name = "KeyText")
    private String keyText;     //cant be null
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
    @XmlElement(name = "ValueText")
    private String valueText;   //cant be null
}
