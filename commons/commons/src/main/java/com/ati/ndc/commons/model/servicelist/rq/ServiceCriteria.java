package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ServiceCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceCriteria {
    @XmlElement(name = "IncludeInd")
    private Boolean includeInd;
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;
    //    Reason For Issuance Code
    //    Examples:
    //    A (Air Transportation)
    //    C (Baggage)
    //    E (Airport Services)
    //    F (Merchandise)
    //    G (In-flight Services)
    //    Refer to PADIS Codeset for data element 4183 - Special Condition.
    @XmlElement(name = "RFIC")
    private String rfic; //with value [0-9A-Z]{1,3}

    //    Reason For Issuance Sub Code.
    //    Examples:
    //    0CC (First Checked Bag)
    //    0B1 (In-flight Entertainment)
    //    0BX (Lounge)
    @XmlElement(name = "RFISC")
    private List<String> rfisc; //max 99 elements
    @XmlElement(name = "ServicePricingOnlyInd")
    private Boolean servicePricingOnlyInd;
    @XmlElement(name = "TaxonomyCode")
    private List<String> taxonomyCode;
}