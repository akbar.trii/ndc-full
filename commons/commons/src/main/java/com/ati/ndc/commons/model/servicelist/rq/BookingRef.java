package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "BookingRef")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingRef {
    @XmlElement(name = "BookingEntity")
    private BookingEntity bookingEntity;
    @XmlElement(name = "BookingID")
    private String bookingId;   //cant be null
    @XmlElement(name = "BookingRefTypeCode")
    private String bookingRefTypeCode;
}