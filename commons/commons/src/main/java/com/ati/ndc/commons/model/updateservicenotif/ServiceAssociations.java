package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ServiceAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceAssociations {
    //choices
    @XmlElement(name = "PaxSegmentRef")
    private PaxSegmentRef paxSegmentRef;
    @XmlElement(name = "ServiceDefinitionRef")
    private ServiceDefinitionRef serviceDefinitionRef;
}