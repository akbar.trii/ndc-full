package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "PrePaidCertificate")
@XmlAccessorType(XmlAccessType.FIELD)
public class PrePaidCertificate {
    @XmlElement(name = "AppText")
    private String appText;
    @XmlElement(name = "EffectiveDateTime")
    private Date effectiveDateTime;
    @XmlElement(name = "ExpiryDateTime")
    private Date expiryDateTime;
    @XmlElement(name = "Number")
    private Double number;
}
