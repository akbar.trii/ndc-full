package com.ati.ndc.commons.model.orderlist.rs;

import com.ati.ndc.commons.model.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_OrderListRS {
    // one of these element shud has value (choice)
    private List<Error> error;
    private Response response;
    //end

    private String augmentationPoint;   //any element // still wrong datatype
//    private MessageDoc messageDoc;
//    private PayloadStandardAttributes payloadAttributes;
}