package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "Service")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Service {
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ServiceID")
    private String serviceID; //cant be null
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
}
