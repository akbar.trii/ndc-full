package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.TaxExemptionQualificationCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "Employer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employer {
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "SalesTaxRegistration")
    private List<SalesTaxRegistration> salesTaxRegistration;
    @XmlElement(name = "TaxationExemptionQualifierCode")
    private TaxExemptionQualificationCodeType taxationExemptionQualifierCode;
    @XmlElement(name = "TaxdeductedatSourceCapabilityInd")
    private Boolean taxdeductedatSourceCapabilityInd;   //cant be null
}
