package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "PaymentTimeLimit")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTimeLimit {
    @XmlElement(name = "PaymentTimeLimitDateTime")
    private Date paymentTimeLimitDateTime;
    @XmlElement(name = "PaymentTimeLimitDuration")
    private String paymentTimeLimitDuration;
}
