package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.Duration;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Visa")
@XmlAccessorType(XmlAccessType.FIELD)
public class Visa {
    @XmlElement(name = "EnterBeforeDate")
    private Date enterBeforeDate;
    @XmlElement(name = "EntryQty")
    private Double entryQty;
    @XmlElement(name = "HostCountryCode")
    private String hostCountryCode; //token with value pattern [A-Z]{2}
    @XmlElement(name = "StayDuration")
    private Duration stayDuration;
    @XmlElement(name = "VisaID")
    private String visaId;
    @XmlElement(name = "VisaTypeCode")
    private String visaTypeCode;
}
