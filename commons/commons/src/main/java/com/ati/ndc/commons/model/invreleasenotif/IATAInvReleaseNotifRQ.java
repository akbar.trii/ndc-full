package com.ati.ndc.commons.model.invreleasenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_InvReleaseNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATAInvReleaseNotifRQ {
    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //any element // still wrong datatype
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Notification")
    private Notification Notification;    //cant be null
    @XmlElement(name = "Party")
    private Party party;    //cant be null and recipient cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
}
