package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxSegment")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxSegment {
    @XmlElement(name = "ARNK_Ind")
    private Boolean arnkInd;
    @XmlElement(name = "Arrival")
    private Arrival arrival;    //cant be null
    @XmlElement(name = "CabinType")
    private CabinType cabinType;
    @XmlElement(name = "DatedOperatingLeg")
    private List<DatedOperatingLeg> datedOperatingLeg;
    @XmlElement(name = "Dep")
    private Arrival dep;    //cant be null
    @XmlElement(name = "Duration")
    private String duration;
    @XmlElement(name = "InterlineSettlementInfo")
    private InterlineSettlementInfo interlineSettlementInfo;
    @XmlElement(name = "MarketingCarrierInfo")
    private MarketingCarrierInfo marketingCarrierInfo;  //cant be null
    @XmlElement(name = "MarketingCarrierRBD_Code")
    private String marketingCarrierRBDCode;
    @XmlElement(name = "OperatingCarrierInfo")
    private OperatingCarrierInfo operatingCarrierInfo;
    @XmlElement(name = "OperatingCarrierRBD_Code")
    private String operatingCarrierRBDCode;
    @XmlElement(name = "PaxSegmentID")
    private String paxSegmentId;    //cant be null
    @XmlElement(name = "SecureFlightInd")
    private Boolean secureFlightInd;
    @XmlElement(name = "SegmentTypeCode")
    private String segmentTypeCode;
    @XmlElement(name = "TicketlessInd")
    private Boolean ticketlessInd;
}
