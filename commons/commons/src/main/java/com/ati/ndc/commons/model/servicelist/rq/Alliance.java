package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Alliance")
@XmlAccessorType(XmlAccessType.FIELD)
public class Alliance {
    @XmlElement(name = "AllianceCode")
    private String allianceCode;    //token with length 3
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "URL")
    private String url;
}