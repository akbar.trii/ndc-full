package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRedirection {
    private Date expirationDateTime;
    private Boolean paymentRedirectionInd;
    private Boolean redirectionAcceptanceInd;   //cant be null
}
