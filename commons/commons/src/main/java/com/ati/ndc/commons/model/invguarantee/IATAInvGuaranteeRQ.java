package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "IATA_InvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATAInvGuaranteeRQ {
    @XmlAttribute
    private String xmlns;
    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Party")
    private Party party;
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "Request")
    private Request request;    //cant be null
}