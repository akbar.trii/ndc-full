package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Eligibility")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Eligibility {
    @XmlElement(name = "EligibilityFlightAssociations")
    private EligibilityFlightAssociations eligibilityFlightAssociations;
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;
    @XmlElement(name = "PriceClassRefID")
    private List<String> priceClassRefId;
}
