package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Eligibility")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FlightAssociations {
    //choices
    @XmlElement(name = "DatedOperatingLegRefId")
    private List<String> datedOperatingLegRefId;    //cant be null
    @XmlElement(name = "PaxJourneyRefId")
    private List<String> paxJourneyRefId;   //cant be null
    @XmlElement(name = "PaxSegmentRefId")
    private List<String> paxSegmentRefId;   //cant be null
}
