package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "AggregatorAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class AggregatorAirlineProfileNotifRQ {
    @XmlElement(name = "AggregatorID")
    private String AggregatorId;    //cant be null
    @XmlElement(name = "ContactInfo")
    private List<ContactInfoAirlineProfileNotifRQ> contactInfo;
    @XmlElement(name = "Name")
    private String name;
}
