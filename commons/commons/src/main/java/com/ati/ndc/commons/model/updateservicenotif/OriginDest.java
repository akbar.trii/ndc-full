package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OriginDest")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OriginDest {
    @XmlElement(name = "DestCode")
    private String destCode;    //token with length 3 and cant be null
    @XmlElement(name = "OriginCode")
    private String originCode;  //token with length 3 and cant be null
    @XmlElement(name = "OriginDestID")
    private String originDestId;
    @XmlElement(name = "PaxJourneyRefID")
    private List<String> paxJourneyRefId;
}