package com.ati.ndc.commons.model.paymentclearance.rs;

import com.ati.ndc.commons.model.ClearanceTypePaymentClearanceRS;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceRS {
    // choice
    private List<ClearanceTypePaymentClearanceRS> clearance;
    private Error error;
    //end
//    private PayloadStandardAttributes payloadStandardAttributes;
}