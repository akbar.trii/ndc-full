package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "LoyaltyProgramAccountRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoyaltyProgramAccountRQ {
    @XmlElement(name = "AccountNumber")
    private String accountNumber;
    @XmlElement(name = "Alliance")
    private Alliance alliance;
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "LoyaltyProgram")
    private LoyaltyProgram loyaltyProgram;  //cant be null
    @XmlElement(name = "SignInID")
    private String signInId;
    @XmlElement(name = "TierCode")
    private String tierCode;
    @XmlElement(name = "TierName")
    private String tierName;
    @XmlElement(name = "TierPriorityText")
    private String tierPriorityText;
}