package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "PromotionType")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromotionType {
    @XmlElement(name = "OwnerName")
    private String ownerName;
    @XmlElement(name = "PaxRefID")
    private String paxRefId;
    @XmlElement(name = "PromotionID")
    private String promotionId;
    @XmlElement(name = "PromotionIssuer")
    private PromotionIssuerRS promotionIssuer;    //sequential not choices
    @XmlElement(name = "Remark")
    private List<Remark> remark;
    @XmlElement(name = "URL")
    private String url;
}
