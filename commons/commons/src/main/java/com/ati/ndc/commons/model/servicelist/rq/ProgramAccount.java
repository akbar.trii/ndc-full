package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ProgramAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProgramAccount {
    @XmlElement(name = "AccountID")
    private String accountId;   //cant be null
}