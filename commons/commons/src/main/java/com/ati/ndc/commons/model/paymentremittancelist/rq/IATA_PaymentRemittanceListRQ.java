package com.ati.ndc.commons.model.paymentremittancelist.rq;

import com.ati.ndc.commons.model.RemittanceDate;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentRemittanceListRQ {
    //choose 1 value between this
    private Integer remitID;
    private RemittanceDate remittanceDate;
    // end

//    private PayloadStandardAttributes payloadStandardAttributes;
}