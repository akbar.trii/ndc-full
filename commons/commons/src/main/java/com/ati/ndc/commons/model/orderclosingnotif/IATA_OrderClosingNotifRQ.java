package com.ati.ndc.commons.model.orderclosingnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_OrderClosingNotifRQ {
    private String augmentationPoint;   //value is any element, still wrong datatype and cant be null
//    private Party party;  //cant be null
//    private PayloadStandardAttributes payloadAttributes;
//    private Request request;    //cant be null
}