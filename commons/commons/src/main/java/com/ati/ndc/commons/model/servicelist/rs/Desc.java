package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Desc")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Desc {
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "MarkupStyleText")
    private String markupStyleText;
    @XmlElement(name = "MediaObject")
    private MediaObject mediaObject;
    @XmlElement(name = "URL")
    private String url;
}