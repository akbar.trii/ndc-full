package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxSegmentListRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxSegmentListRQ {
        @XmlElement(name = "PaxSegment")
        private List<PaxSegmentRQ> paxSegment;   //cant be null
}
