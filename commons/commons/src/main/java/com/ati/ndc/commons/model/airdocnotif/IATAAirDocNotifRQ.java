package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_AirDocNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATAAirDocNotifRQ {
    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //any element // still wrong datatype
    @XmlElement(name = "DocNotification")
    private DocNotification docNotification;    //cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Party")
    private Party party;    //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
}