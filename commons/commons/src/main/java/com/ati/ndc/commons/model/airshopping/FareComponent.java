package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareComponent")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareComponent {
    @XmlElement(name = "CabinType")
    private CabinType cabinType;
    @XmlElement(name = "FareBasisAppCode")
    private String fareBasisAppCode;
    @XmlElement(name = "FareBasisCityPairText")
    private String fareBasisCityPairText;
    @XmlElement(name = "FareBasisCode")
    private String fareBasisCode;
    @XmlElement(name = "FareRule")
    private List<FareRule> fareRule;
    @XmlElement(name = "FareTypeCode")
    private String fareTypeCode;
    @XmlElement(name = "NegotiatedCode")
    private String negotiatedCode;
    @XmlElement(name = "PaxSegmentRefID")
    private List<String> paxSegmentRefId;
    @XmlElement(name = "Price")
    private PriceType price;
    @XmlElement(name = "PriceClassRefID")
    private String priceClassRefId;
    @XmlElement(name = "RBD")
    private RBD rbd;
    @XmlElement(name = "TicketDesigCode")
    private String ticketDesigCode;
}
