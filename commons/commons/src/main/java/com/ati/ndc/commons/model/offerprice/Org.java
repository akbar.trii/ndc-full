package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Org")
@XmlAccessorType(XmlAccessType.FIELD)
public class Org {
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "Name")
    private String name;    //cant be null
    @XmlElement(name = "OrgID")
    private String orgId;   //cant be null
}
