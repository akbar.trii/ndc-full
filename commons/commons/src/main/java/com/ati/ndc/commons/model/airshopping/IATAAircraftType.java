package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "IATAAircraftType")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAAircraftType {
    @XmlElement(name = "IATA_AircraftTypeCode")
    private String IATAAircraftTypeCode;    // token with value pattern [0-9A-Z]{3}
}
