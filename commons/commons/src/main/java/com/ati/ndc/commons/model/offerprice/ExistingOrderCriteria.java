package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ExistingOrderCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExistingOrderCriteria {
    @XmlElement(name = "Order")
    private Order order;   //cant be null
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;    //cant be null
}