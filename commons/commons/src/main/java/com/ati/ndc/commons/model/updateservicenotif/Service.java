package com.ati.ndc.commons.model.updateservicenotif;

import com.ati.ndc.commons.model.enumeration.ServiceBookingStatusCodeType;
import com.ati.ndc.commons.model.enumeration.ServiceDeliveryStatusCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "Service")
@XmlAccessorType(XmlAccessType.FIELD)
public class Service {
    @XmlElement(name = "Bag")
    private Bag bag;
    @XmlElement(name = "BookingStatusCode")
    private ServiceBookingStatusCodeType bookingStatusCode;
    @XmlElement(name = "DeliveryMilestoneCode")
    private String deliveryMilestoneCode;
    @XmlElement(name = "DeliveryProvider")
    private DeliveryProvider deliveryProvider;  //cant be null
    @XmlElement(name = "DeliveryStatusCode")
    private ServiceDeliveryStatusCodeType deliveryStatusCode;
    @XmlElement(name = "DeliveryStatusDescText")
    private String deliveryStatusDescText;
    @XmlElement(name = "ExpirationDateTime")
    private Date expirationDateTime;
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ResponsibleAirline")
    private Carrier responsibleAirline;
    @XmlElement(name = "ServiceAssociations")
    private ServiceAssociations serviceAssociations;    //cant be null
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
    @XmlElement(name = "ValidatingCarrier")
    private Carrier validatingCarrier;
}
