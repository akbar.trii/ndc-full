package com.ati.ndc.commons.model.updateservicenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "EmailAddress")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailAddress {
    @XmlElement(name = "ContactTypeText")
    private String contactTypeText;
    @XmlElement(name = "EmailAddressText")
    private String emailAddressText;    //cant be null
}