package com.ati.ndc.commons.model.updateservicenotif;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "IATA_UpdateServiceNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAUpdateServiceNotifRQ {
    @XmlElement(name = "AugmentationPoint")
    private String augmentationPoint;
    @XmlElement(name = "Party")
    private Party party;    //cant be null
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    @XmlElement(name = "Request")
    private Request request;    //cant be null

    //lihat xml lagi karna masih ada tambahan
}
