package com.ati.ndc.commons.model.enumeration;

public enum BaggageTypeCodeType {
    CarryOn,
    Checked
}
