package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "AirlineProfileDataItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AirlineProfileDataItem {
    @XmlElement(name = "ActionTypeCode")
    private String actionTypeCode;
//    @XmlElement(name = "OfferFilterCriteria")
//    private List<OfferFilterCriteria> offerFilterCriteria;
    @XmlElement(name = "POS_FilterCriteria")
    private List<POSFilterCriteria> posFilterCriteria;
//    @XmlElement(name = "POS_GeographicFilterCriteria")
//    private List<POSGeographicFilterCriteria> posGeographicFilterCriteria;
    @XmlElement(name = "SeqNumber")
    private Integer seqNumber;
//    @XmlElement(name = "ServiceCriteria")
//    private List<ServiceCriteria> serviceCriteria;
}
