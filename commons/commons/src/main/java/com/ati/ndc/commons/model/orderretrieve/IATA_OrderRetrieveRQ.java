package com.ati.ndc.commons.model.orderretrieve;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_OrderRetrieveRQ extends IATA_OrderChangeRQ {
}