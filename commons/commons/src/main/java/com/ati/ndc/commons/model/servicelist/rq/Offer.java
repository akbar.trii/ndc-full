package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Offer")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Offer {
    @XmlElement(name = "OfferID")
    private String offerId;
    @XmlElement(name = "OfferItem")
    private List<OfferItem> offerItem;
    @XmlElement(name = "OwnerCode")
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "WebAddressURL")
    private String webAddressURL;
}