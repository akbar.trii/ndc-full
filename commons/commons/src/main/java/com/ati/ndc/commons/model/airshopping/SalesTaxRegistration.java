package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "SalesTaxRegistration")
@XmlAccessorType(XmlAccessType.FIELD)
public class SalesTaxRegistration {
    @XmlElement(name = "CityName")
    private String cityName;
    @XmlElement(name = "CountryCode")
    private String countryCode; //token with pattern value [A-Z]{2}
    @XmlElement(name = "CountryDialingCode")
    private String countryDialingCode;
    @XmlElement(name = "CountrySubDivisionName")
    private String countrySubDivisionName;
    @XmlElement(name = "EmailAddressText")
    private String emailAddressText;
    @XmlElement(name = "PhoneNumber")
    private List<String> phoneNumber;   //max 2 element
    @XmlElement(name = "PostalCode")
    private String postalCode;
    @XmlElement(name = "SalesTaxRegistrationID")
    private String salesTaxRegistrationId;
    @XmlElement(name = "StreetText")
    private List<String> streetText;  //max 2 element
}