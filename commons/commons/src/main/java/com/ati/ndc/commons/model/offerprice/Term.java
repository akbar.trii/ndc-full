package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Term")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Term {
    @XmlElement(name = "AvailPeriod")
    private AvailPeriod availPeriod;    //cant be null
    @XmlElement(name = "Desc")
    private Desc desc;
    @XmlElement(name = "OrderingQty")
    private OrderingQty orderingQty;
    @XmlElement(name = "TermID")
    private String termId;  //cant be null
}
