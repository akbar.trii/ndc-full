package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "JourneyOverview")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class JourneyOverview {
    @XmlElement(name = "JourneyPriceClass")
    private List<JourneyPriceClass> journeyPriceClass;    //cant be null
    @XmlElement(name = "PriceClassRefID")
    private String priceClassRefId;
}
