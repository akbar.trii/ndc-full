package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ProgramOwner")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProgramOwner {
    @XmlElement(name = "CompanyIndexText")
    private Carrier carrier;    //cant be null
    private Org org;    //cant be null
}
