package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.paymentclearancelist.IATA_EasyPay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentMethod {
    // choices, must pick one of these
    private AccountableDoc accountableDoc;
    private BankTransfer bankTransfer;
    private Cash cash;
    private Check check;
    private DirectBill directBill;
    private IATA_EasyPay iataEasyPay;
    private LoyaltyRedemption loyaltyRedemption;
    private OtherPaymentMethod otherPaymentMethod;
    private PaymentCard paymentCard;
    private Voucher voucher;
}