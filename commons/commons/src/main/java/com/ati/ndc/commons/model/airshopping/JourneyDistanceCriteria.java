package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.PrefLevelCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "JourneyDistanceCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class JourneyDistanceCriteria {
    @XmlElement(name = "DistanceMeasure")
    private Double distanceMeasure;
    @XmlElement(name = "PrefCode")
    private PrefLevelCodeType prefCode;
}
