package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PriceCalendar")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PriceCalendar {
    @XmlElement(name = "LeadPriceInd")
    private Boolean leadPriceInd;
    @XmlElement(name = "PriceCalendarDate")
    private List<PriceCalendarDate> priceCalendarDate;    //cant be null
    @XmlElement(name = "PricedPTC")
    private List<PTCOfferParameters> pricedPTC;
    @XmlElement(name = "TotalPriceAmount")
    private Double totalPriceAmount;
}
