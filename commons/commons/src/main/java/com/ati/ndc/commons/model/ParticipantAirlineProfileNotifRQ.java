package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ParticipantAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParticipantAirlineProfileNotifRQ {
    @XmlElement(name = "Aggregator")
    private AggregatorAirlineProfileNotifRQ aggregator;
    @XmlElement(name = "Corporation")
    private CorporationAirlineProfileNotifRQ corporation;
    @XmlElement(name = "EnabledSystem")
    private EnabledSystemAirlineProfileNotifRQ enabledSystem;
    @XmlElement(name = "MarketingCarrier")
    private CarrierAirlineProfileNotifRQ marketingCarrier;
    @XmlElement(name = "OperatingCarrier")
    private CarrierAirlineProfileNotifRQ operatingCarrier;
    @XmlElement(name = "ORA")
    private CarrierAirlineProfileNotifRQ ora;
    @XmlElement(name = "POA")
    private CarrierAirlineProfileNotifRQ poa;
    @XmlElement(name = "RetailPartner")
    private RetailPartnerAirlineProfileNotifRQ retailPartner;
    @XmlElement(name = "TravelAgency")
    private TravelAgencyAirlineProfileNotifRQ travelAgency;
}
