package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ALaCarteOfferItemFlightAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AlaCarteOfferItemFlightAssociations {
    //choices
    @XmlElement(name = "DatedOperatingLegRef")
    private DatedOperatingLegRef datedOperatingLegRef;
    @XmlElement(name = "PaxJourneyRef")
    private PaxJourneyRef paxJourneyRef;
    @XmlElement(name = "PaxSegmentRef")
    private PaxSegmentRef paxSegmentRef;
}
