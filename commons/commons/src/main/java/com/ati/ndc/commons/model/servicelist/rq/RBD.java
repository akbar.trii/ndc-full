package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RBD")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class RBD {
    @XmlElement(name = "RBD_Code")
    private String rbdCode; //cant be null
}
