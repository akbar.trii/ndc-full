package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CarrierAircraftType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class CarrierAircraftType {
    @XmlElement(name = "CarrierAircraftTypeCode")
    private String carrierAircraftTypeCode;
    @XmlElement(name = "CarrierAircraftTypeName")
    private String carrierAircraftTypeName;
}