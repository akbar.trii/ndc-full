package com.ati.ndc.commons.model.enumeration;

public enum TypeCode {
    OnlineTravelAgency,
    TravelAgency,
    TravelManagementCompany
}
