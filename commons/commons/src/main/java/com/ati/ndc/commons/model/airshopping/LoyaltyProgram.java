package com.ati.ndc.commons.model.airshopping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LoyaltyProgram")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyProgram {
    @XmlElement(name = "Alliance")
    private Alliance alliance;
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "ProgramCode")
    private String programCode;
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "ProviderName")
    private String providerName;
    @XmlElement(name = "URL")
    private String url;
}