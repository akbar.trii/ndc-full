package com.ati.ndc.commons.model.airdocnotif;

import com.ati.ndc.commons.model.enumeration.GenderCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Individual")
@XmlAccessorType(XmlAccessType.FIELD)
public class Individual {
    @XmlElement(name = "AddlName")
    private List<AddlName> addlName;
    @XmlElement(name = "Birthdate")
    private Date birthdate;
    @XmlElement(name = "BirthplaceText")
    private String birthplaceText;
    @XmlElement(name = "GenderCode")
    private GenderCode genderCode;
    @XmlElement(name = "GivenName")
    private List<String> givenName;     //max 5 elements
    @XmlElement(name = "IndividualID")
    private String individualId;
    @XmlElement(name = "MiddleName")
    private List<String> middleName;    //max 3 elements
    @XmlElement(name = "SuffixName")
    private String suffixName;
    @XmlElement(name = "Surname")
    private String surname; //cant be null
    @XmlElement(name = "TitleName")
    private String titleName;
}
