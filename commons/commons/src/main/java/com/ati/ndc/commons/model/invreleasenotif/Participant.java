package com.ati.ndc.commons.model.invreleasenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Participant")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participant {
    @XmlElement(name = "Aggregator")
    private Aggregator aggregator;
    @XmlElement(name = "Corporation")
    private Corporation corporation;
    @XmlElement(name = "EnabledSystem")
    private EnabledSystem enabledSystem;
    @XmlElement(name = "MarketingCarrier")
    private Carrier marketingCarrier;
    @XmlElement(name = "OperatingCarrier")
    private Carrier operatingCarrier;
    @XmlElement(name = "ORA")
    private Carrier ora;
    @XmlElement(name = "POA")
    private Carrier poa;
    @XmlElement(name = "RetailPartner")
    private RetailPartner retailPartner;
    @XmlElement(name = "TravelAgency")
    private TravelAgency travelAgency;
}
