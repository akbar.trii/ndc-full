package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "AvailPeriod")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AvailPeriod {
    @XmlElement(name = "EarliestAvailablePeriodDateTime")
    private Date earliestAvailablePeriodDateTime;
    @XmlElement(name = "LatestAvailablePeriodDateTime")
    private Date latestAvailablePeriodDateTime;
}
