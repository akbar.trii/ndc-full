package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "FareCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class FareCriteria {
    @XmlElement(name = "FareBasisCode")
    private List<String> fareBasisCode;
    @XmlElement(name = "FareTypeCode")
    private List<String> fareTypeCode;
    @XmlElement(name = "GroupFareCriteria")
    private List<com.ati.ndc.commons.model.offerprice.GroupFareCriteria> GroupFareCriteria;
    @XmlElement(name = "NoAdvancePurchaseInd")
    private Boolean noAdvancePurchaseInd;
    @XmlElement(name = "NoMaxStayInd")
    private Boolean noMaxStayInd;
    @XmlElement(name = "NoMinStayInd")
    private Boolean noMinStayInd;
    @XmlElement(name = "NoPenaltyInd")
    private Boolean noPenaltyInd;
    @XmlElement(name = "PrefLevel")
    private PrefLevel prefLevel;    //cant be null
}