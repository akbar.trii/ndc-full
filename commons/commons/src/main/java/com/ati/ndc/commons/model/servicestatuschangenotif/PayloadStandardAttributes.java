package com.ati.ndc.commons.model.servicestatuschangenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PayloadStandardAttributes")
@XmlAccessorType(XmlAccessType.FIELD)
public class PayloadStandardAttributes {
    @XmlElement(name = "AltLangID")
    private String altLangId;
    @XmlElement(name = "CorrelationID")
    private String correlationId;
    @XmlElement(name = "EchoTokenText")
    private String echoTokenText;
    @XmlElement(name = "PrimaryLangID")
    private String primaryLangId;
    @XmlElement(name = "RetransmissionInd")
    private Boolean retransmissionInd;
    @XmlElement(name = "SeqNumber")
    private Integer seqNumber;
    @XmlElement(name = "Timestamp")
    private Timestamp timestamp;
    @XmlElement(name = "TrxID")
    private String trxId;
    @XmlElement(name = "TrxStatusCode")
    private String trxStatusCode;
    @XmlElement(name = "VersionNumber")
    private Double versionNumber;
}