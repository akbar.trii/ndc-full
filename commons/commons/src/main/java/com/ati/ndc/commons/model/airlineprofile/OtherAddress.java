package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "OtherAddress")
@XmlAccessorType(XmlAccessType.FIELD)
public class OtherAddress {
    @XmlElement(name = "ContactTypeText")
    private String contactTypeText;
    @XmlElement(name = "OtherAddressText")
    private String otherAddressText;    //cant be null
}