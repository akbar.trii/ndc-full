package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceDefinitionListRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinitionListRS {
    @XmlElement(name = "ServiceDefinition")
    private List<ServiceDefinitionRS> serviceDefinition;  //cant be null
}
