package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.MatchTypeCode;
import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Offer1")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Offer1 {
    @XmlElement(name = "ALaCarteOfferItem")
    private List<AlaCarteOfferItem> aLaCarteOfferItem;   //cant be null
    @XmlElement(name = "BaggageDisclosureRefID")
    private List<String> baggageDisclosureRefId;
    @XmlElement(name = "Commission")
    private Commission commission;
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "DisclosureRefID")
    private String disclosureRefId;
    @XmlElement(name = "MatchAppText")
    private String matchAppText;
    @XmlElement(name = "MatchPercent")
    private Double matchPercent;
    @XmlElement(name = "MatchTypeCode")
    private MatchTypeCode matchTypeCode;
    @XmlElement(name = "OfferExpirationTimeLimitDateTime")
    private Date offerExpirationTimeLimitDateTime;
    @XmlElement(name = "OfferID")
    private String offerId; //cant be null
    @XmlElement(name = "OwnerCode")
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "PenaltyRefID")
    private List<String> penaltyRefId;
    @XmlElement(name = "PTC_OfferParameters")
    private List<PTCOfferParameters> ptcOfferParameters;
    @XmlElement(name = "RedemptionInd")
    private Boolean redemptionInd;
    @XmlElement(name = "RequestedDateInd")
    private Boolean requestedDateInd;
    @XmlElement(name = "TotalPrice")
    private Double totalPrice;
    @XmlElement(name = "ValidatingCarrierCode")
    private String validatingCarrierCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "WebAddressURL")
    private String webAddressURL;
}
