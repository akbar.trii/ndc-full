package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "RequestInvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestInvGuaranteeRQ {
//    @XmlElement(name = "BookingRef")
//    private List<BookingRef> bookingRef;
    @XmlElement(name = "Offer")
    private OfferInvGuaranteeRQ offer;
    @XmlElement(name = "Order")
    private OrderInvGuaranteeRQ order;
//    @XmlElement(name = "Qualifiers")
//    private Qualifiers qualifiers;
//    @XmlElement(name = "ShoppingResponse")
//    private ShoppingResponseInvGuaranteeRQ shoppingResponse;
}
