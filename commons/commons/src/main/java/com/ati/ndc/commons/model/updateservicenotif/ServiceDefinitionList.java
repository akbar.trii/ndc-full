package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceDefinitionList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ServiceDefinitionList {
    @XmlElement(name = "ServiceDefinition")
    private List<ServiceDefinition> serviceDefinition;  //cant be null
}