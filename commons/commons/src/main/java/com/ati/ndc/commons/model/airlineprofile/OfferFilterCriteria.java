package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OfferFilterCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferFilterCriteria {
    //choices
    @XmlElement(name = "DirectionalIndText")
    private List<DirectionalIndTextType> directionalIndTextType;  // example value (1 = From LOC 1 to LOC 2, 2 = To LOC 1 from LOC 2, 3 = Both)
    @XmlElement(name = "OfferDestPoint")
    private List<POSGeographicFilterCriteria> offerDestPoint;
    @XmlElement(name = "OfferOriginPoint")
    private List<POSGeographicFilterCriteria> offerOriginPoint;
    @XmlElement(name = "TravelWithin")
    private List<POSGeographicFilterCriteria> travelWithin;
}