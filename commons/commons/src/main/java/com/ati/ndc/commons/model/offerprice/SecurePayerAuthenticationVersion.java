package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "SecurePayerAuthenticationVersion")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurePayerAuthenticationVersion {
    @XmlElement(name = "CardEnrollmentVersionText")
    private List<String> cardEnrollmentVersionText;
    @XmlElement(name = "SupportedVersionText")
    private List<String> supportedVersionText;  //cant be null
}
