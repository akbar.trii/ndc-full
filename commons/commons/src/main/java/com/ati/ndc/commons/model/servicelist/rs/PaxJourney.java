package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxJourney")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxJourney {
    @XmlElement(name = "DistanceMeasure")
    private Double distanceMeasure;
    @XmlElement(name = "Duration")
    private String duration;
    @XmlElement(name = "InterlineSettlementInfo")
    private InterlineSettlementInfo interlineSettlementInfo;
    @XmlElement(name = "PaxJourneyID")
    private String paxJourneyId;
    @XmlElement(name = "PaxSegmentRefID")
    private List<String> paxSegmentRefId;   //cant be null
}
