package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RSAKeyValueType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class RSAKeyValueType {
    @XmlElement(name = "Exponent")
    private byte[] exponent;    //cant be null
    @XmlElement(name = "Modulus")
    private byte[] modulus; //cant be null
}
