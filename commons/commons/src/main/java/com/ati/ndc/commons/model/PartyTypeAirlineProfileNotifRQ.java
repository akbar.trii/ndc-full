package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PartyTypeAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PartyTypeAirlineProfileNotifRQ {
    //1 of element should has value (choice)
    @XmlElement(name = "Aggregator")
    private AggregatorAirlineProfileNotifRQ aggregator;
    @XmlElement(name = "Corporation")
    private CorporationAirlineProfileNotifRQ corporation;
    @XmlElement(name = "EnabledSystem")
    private EnabledSystemAirlineProfileNotifRQ enabledSystem;
    @XmlElement(name = "MarketingCarrier")
    private CarrierAirlineProfileNotifRQ marketingCarrier;
    @XmlElement(name = "OperatingCarrier")
    private CarrierAirlineProfileNotifRQ operatingCarrier;
    @XmlElement(name = "ORA")
    private CarrierAirlineProfileNotifRQ ora;
    @XmlElement(name = "POA")
    private CarrierAirlineProfileNotifRQ poa;
    @XmlElement(name = "RetailPartner")
    private RetailPartnerAirlineProfileNotifRQ retailPartner;
    @XmlElement(name = "TravelAgency")
    private TravelAgencyAirlineProfileNotifRQ travelAgency;
}