package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxJourneyRef")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxJourneyRef {
    @XmlElement(name = "PaxJourneyRefID")
    private List<String> paxJourneyRefId;   //cant be null
}
