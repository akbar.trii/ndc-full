package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.PaymentStatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingSummary {
    private Double amount; //cant be null
    private String contactInfoRefId;    //token
//    private Desc desc;
    private MerchantAccount merchantAccount;
//    private OrderAssociation orderAssociation;
    private Payer payer;
    private Date paymentCommitmentDateTime;
    private String paymentId;   //token and cant be null
    private PaymentMethod paymentMethod;    //cant be null
    private String paymentRefId;   //token
    private PaymentStatusCode paymentStatusCode;
    private PaymentTrx paymentTrx;
    private Double priceVarianceAmount;
//    private List<Promotion> promotion;
    private Double surchargeAmount;
    private String typeCode;    //token and cant be null
    private Boolean verificationInd;
}