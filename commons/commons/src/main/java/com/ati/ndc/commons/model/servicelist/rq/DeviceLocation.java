package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "DeviceLocation")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeviceLocation {
    @XmlElement(name = "City")
    private City city;
    @XmlElement(name = "Country")
    private Country country;
    @XmlElement(name = "GeospatialLocation")
    private GeoSpatialLocation geospatialLocation;
}