package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DataListsRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class DataListsRQ {
    @XmlElement(name = "BaggageAllowanceList")
    private BaggageAllowanceList baggageAllowanceList;
    @XmlElement(name = "BaggageDisclosureList")
    private BaggageDisclosureList baggageDisclosureList;
    @XmlElement(name = "ContactInfoList")
    private ContactInfoList contactInfoList;
    @XmlElement(name = "DisclosureList")
    private DisclosureList disclosureList;
    @XmlElement(name = "FareList")
    private FareListRQ fareList;
    @XmlElement(name = "MediaList")
    private MediaList mediaList;
    @XmlElement(name = "OriginDestList")
    private OriginDestList originDestList;
    @XmlElement(name = "PaxJourneyList")
    private PaxJourneyListRQ paxJourneyList;
    @XmlElement(name = "PaxList")
    private PaxListRQ paxList;
    @XmlElement(name = "PaxSegmentList")
    private PaxSegmentListRQ paxSegmentList;
    @XmlElement(name = "PenaltyList")
    private PenaltyListRQ penaltyList;
    @XmlElement(name = "PriceClassList")
    private PriceClassList priceClassList;
    @XmlElement(name = "SeatProfileList")
    private SeatProfileList seatProfileList;
    @XmlElement(name = "ServiceDefinitionList")
    private ServiceDefinitionListRQ serviceDefinitionList;
    @XmlElement(name = "TermsList")
    private TermsList termsList;
}