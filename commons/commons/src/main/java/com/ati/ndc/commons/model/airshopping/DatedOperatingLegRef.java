package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "DatedOperatingLegRef")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class DatedOperatingLegRef {
    @XmlElement(name = "DatedOperatingLegRefID")
    private List<String> datedOperatingLegRefId;  //cant be null
}
