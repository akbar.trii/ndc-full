package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "NotifAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotifAirlineProfileNotifRQ {
    @XmlElement(name = "AirlineProfile")
    private AirlineProfileAirlineProfileNotifRQ airlineProfile;  //cant be null
}