package com.ati.ndc.commons.model.updateservicenotif;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BaggageAllowanceList")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaggageAllowanceList {
    @XmlElement(name = "BaggageAllowance")
    private List<BaggageAllowance> baggageAllowance;    //cant be null
}
