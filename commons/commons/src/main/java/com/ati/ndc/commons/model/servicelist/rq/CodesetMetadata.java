package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CodesetMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodesetMetadata {
    @XmlElement(name = "CodesetName")
    private String codesetName;
    @XmlElement(name = "CodesetVersionNumber")
    private Integer codesetVersionNumber;
    @XmlElement(name = "LangUsage")
    private LangUsage langUsage;
    @XmlElement(name = "OwnerId")
    private String ownerId;
    @XmlElement(name = "OwnerName")
    private String ownerName;
}