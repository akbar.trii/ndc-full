package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ServiceFeature")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceFeature {
    @XmlElement(name = "CodesetCode")
    private String codesetCode;
    @XmlElement(name = "CodesetNameCode")
    private String codesetNameCode;
    @XmlElement(name = "ValueText")
    private String valueText;
}
