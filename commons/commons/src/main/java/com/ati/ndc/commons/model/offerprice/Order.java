package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.OrderStatusCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {
    @XmlElement(name = "BookingRef")
    private List<BookingRef> bookingRef;
    @XmlElement(name = "Commission")
    private List<Commission> commission;
    @XmlElement(name = "CreationDateTime")
    private Date creationDateTime;
    @XmlElement(name = "DepositTimeLimitDateTime")
    private Date depositTimeLimitDateTime;
    @XmlElement(name = "LastModifiedDateTime")
    private Date lastModifiedDateTime;
    @XmlElement(name = "OrderItem")
    private List<OrderItem> orderItem;
    @XmlElement(name = "OrderVersionNumber")
    private Integer orderVersionNumber;
    @XmlElement(name = "StatusCode")
    private OrderStatusCodeType statusCode;
}