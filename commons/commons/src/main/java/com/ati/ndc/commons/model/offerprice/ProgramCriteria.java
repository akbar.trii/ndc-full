package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ProgramCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProgramCriteria {
    @XmlElement(name = "CompanyIndexText")
    private String companyIndexText;
    @XmlElement(name = "MemberStatusText")
    private String memberStatusText;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "PrePaidCertificate")
    private List<PrePaidCertificate> prePaidCertificate;
    @XmlElement(name = "ProgamContract")
    private ProgamContract progamContract;  //ContractID can be null
    @XmlElement(name = "ProgramAccount")
    private List<ProgramAccount> programAccount;    //AccountID can be null
    @XmlElement(name = "TypeCode")
    private String typeCode;
}
