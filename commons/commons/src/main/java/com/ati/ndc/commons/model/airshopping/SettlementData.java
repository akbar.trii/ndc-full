package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "SettlementData")
@XmlAccessorType(XmlAccessType.FIELD)
public class SettlementData {
    @XmlElement(name = "NetClearanceAmount")
    private Double netClearanceAmount;  //cant be null
    @XmlElement(name = "RemittanceDate")
    private Date remittanceDate;
}
