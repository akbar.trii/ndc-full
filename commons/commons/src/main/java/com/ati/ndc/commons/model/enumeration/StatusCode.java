package com.ati.ndc.commons.model.enumeration;

public enum StatusCode {
    CANCELLED_BY_CUSTOMER,
    ENTITLED,
    NOT_ENTITLED
}
