package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CryptographyKey")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CryptographyKey {
    @XmlElement(name = "KeyName")
    private String keyName; //cant be null
    @XmlElement(name = "KeyValue")
    private KeyValue keyValue;
}